package com.service;

import java.util.List;

import com.entities.Coupons;

public interface CouponsService {
	//根据查询条件来查询优惠券
	public List<Coupons> getCouponsBuQuery(String startDate ,String expiryDate,Double priceMin,Double priceMax );
	
	//添加优惠券
	public boolean doInsertCoupons(Coupons coupons);
	
	//修改优惠券
	public boolean doUpdateCoupons(Coupons coupons);
	
	//通过id获取优惠券的信息
	public Coupons getCouponsByCouponsId(Integer couponsId);
	
	//删除优惠券
	public boolean doDeleteCoupons(Integer couponsId);
}
