package com.service;

import java.util.List;

import com.entities.Admin;

public interface AdminService {
	//管理员登陆
	public Admin loginAdmin(String adminName,String adminPassword);
	
	//通过账号id获取管理员信息
	public Admin getAdminByAdminId(Integer adminId);
	
	//通过登陆名来查询管理员
	public List<Admin> 	getAdminByAdminNameLike(String adminName);
	
	//添加管理员
	public boolean doInsertAdmin(Admin admin);
	
	//修改管理员
	public boolean doUpdateAdmin(Admin admin);
	
	//删除管理员
	public boolean doDeleteAdmin(Integer adminId);
	
}
