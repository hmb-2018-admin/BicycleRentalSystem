package com.service;

import java.util.List;

import com.entities.Bicycle;

public interface BicycleService {
	//通过查询条件来查询自行车
	public List<Bicycle> getBicycleByQueryCondition(String populationIds,String degreeIds ,String timeIds ,Double priceMin,Double priceMax ,String brand ,String states);
	
	//添加新的自行车
	public boolean doInsertBicycle(Bicycle bicycle );
	
	//修改自行车信息
	public boolean doUpdateBicycle(Bicycle bicycle);
	
	//删除自行车
	public boolean doDeleteBicycle(Integer bicycleId);
	
	//通过id获取自行车信息
	public Bicycle getBicycleByBicycleId(Integer bicycleId);
	
	//修改自行车状态
	public boolean doUpdateStateByBicycleIdAndState(Integer bicycleId ,Integer state);
}
