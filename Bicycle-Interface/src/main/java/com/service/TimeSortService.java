package com.service;

import java.util.List;

import com.entities.TimeSort;

public interface TimeSortService {
	//显示所有的时间分类信息
	public List<TimeSort> getTimeSortAll();
	
	//添加新的分类
	public boolean doInsertTimeSort(TimeSort timeSort);
	
	//修改分类信息
	public boolean doUpdateTimeSort(TimeSort timeSort);
	
	//通过id获取分类信息
	public TimeSort getTimeSortByTimeId(Integer timeId);
	
	//通过id来删除分类
	public boolean doDeleteTimeSort(Integer timeId);
}
