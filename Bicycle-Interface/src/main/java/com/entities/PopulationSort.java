package com.entities;

import java.io.Serializable;

public class PopulationSort implements Serializable{
	private Integer populationId;
	private String sortName;
	private String picture;
	
	public Integer getPopulationId() {
		return populationId;
	}
	public void setPopulationId(Integer populationId) {
		this.populationId = populationId;
	}
	public String getSortName() {
		return sortName;
	}
	public void setSortName(String sortName) {
		this.sortName = sortName;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
}
