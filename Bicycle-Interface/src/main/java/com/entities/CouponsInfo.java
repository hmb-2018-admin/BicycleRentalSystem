package com.entities;

import java.io.Serializable;

public class CouponsInfo implements Serializable{
	private Integer infoId;
	private Integer couponsId;
	private Integer accountId;
	private Integer state;
	public Integer getInfoId() {
		return infoId;
	}
	public void setInfoId(Integer infoId) {
		this.infoId = infoId;
	}
	public Integer getCouponsId() {
		return couponsId;
	}
	public void setCouponsId(Integer couponsId) {
		this.couponsId = couponsId;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
}
