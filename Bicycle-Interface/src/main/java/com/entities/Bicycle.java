package com.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Bicycle implements Serializable{
	private Integer bicycleId;
	private String bicycleNumber;
	private Integer priceId;
	private String brand;
	private Integer state;
	private BicyclePrice bicyclePrice;
	
	public String getBicycleNumber() {
		return bicycleNumber;
	}
	public void setBicycleNumber(String bicycleNumber) {
		this.bicycleNumber = bicycleNumber;
	}
	public BicyclePrice getBicyclePrice() {
		return bicyclePrice;
	}
	public void setBicyclePrice(BicyclePrice bicyclePrice) {
		this.bicyclePrice = bicyclePrice;
	}
	public Integer getBicycleId() {
		return bicycleId;
	}
	public void setBicycleId(Integer bicycleId) {
		this.bicycleId = bicycleId;
	}
	public Integer getPriceId() {
		return priceId;
	}
	public void setPriceId(Integer priceId) {
		this.priceId = priceId;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
}
