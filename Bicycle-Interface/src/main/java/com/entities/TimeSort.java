package com.entities;

import java.io.Serializable;

public class TimeSort implements Serializable{
	private Integer timeId;
	private String timeName;
	public Integer getTimeId() {
		return timeId;
	}
	public void setTimeId(Integer timeId) {
		this.timeId = timeId;
	}
	public String getTimeName() {
		return timeName;
	}
	public void setTimeName(String timeName) {
		this.timeName = timeName;
	}
	
}
