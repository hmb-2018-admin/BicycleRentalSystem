package com.controller;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.Admin;
import com.service.AdminService;
@SessionAttributes
@Controller
@RequestMapping("/admin")
public class AdminController {
	@Reference
	private AdminService adminService;
	
	@RequestMapping(value = "/login")
	@ResponseBody
	public boolean login(String adminName ,String adminPassword,Map<String ,Object> map) {
		Admin admin=this.adminService.loginAdmin(adminName, adminPassword);
		if(null != admin) {
			map.put("admin", admin);
			return true;
		}else {
			return false;
		}
	}
	
	@ResponseBody
	@RequestMapping(value="/getAdminByAdminNameLike")
	public List<Admin> getAdminByAdminNameLike(String adminName){
		return this.adminService.getAdminByAdminNameLike(adminName);
	}
	
	@ResponseBody
	@RequestMapping(value="/doInsertAdmin")
	public boolean doInsertAdmin(Admin admin) {
		admin.setAdminPassword("admin");
		return this.adminService.doInsertAdmin(admin);
	}
	@ResponseBody
	@RequestMapping(value="/doUpdateAdmin")
	public boolean doUpdateAdmin(Admin admin) {
		return this.adminService.doUpdateAdmin(admin);
	}
	@ResponseBody
	@RequestMapping(value="/doDeleteAdmin")
	public boolean doDeleteAdmin(Integer adminId) {
		return this.adminService.doDeleteAdmin(adminId);
	}
	@ResponseBody
	@RequestMapping(value="/getAdminByAdminId")
	public Admin getAdminByAdminId(Integer adminId) {
		return this.adminService.getAdminByAdminId(adminId);
	}
	
	@RequestMapping(value = "/verifyPassword")
	@ResponseBody
	public boolean verifyPassword(String adminName ,String adminPassword) {
		Admin admin=this.adminService.loginAdmin(adminName, adminPassword);
		if(null != admin) {
			return true;
		}else {
			return false;
		}
	}
	
	@RequestMapping(value="/doResetPassword")
	@ResponseBody
	public boolean doResetPassword(Integer adminId) {
		Admin admin=this.adminService.getAdminByAdminId(adminId);
		admin.setAdminPassword("admin");
		return this.doUpdateAdmin(admin);
	}
}
