package com.controller;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.PopulationSort;
import com.service.PopulationSortService;
import com.util.GetFileName;

@Controller
@RequestMapping(value = "/populationSort")
public class PopulationSortController {
	
	@Reference
	private PopulationSortService  populationSortService;
	
	@ResponseBody  
	@RequestMapping(value="/getPopulationSortAll")
	public List<PopulationSort> getPopulationSortAll(){
		return this.populationSortService.getPopulationSortAll();
	}

	@ResponseBody
	@RequestMapping(value="/doInsertPopulationSort")
	public boolean doInsertPopulationSort(HttpServletRequest request, String file,String sortName) {
		String serverPath = request.getSession().getServletContext()
				.getRealPath("/bicycleImg");
		Base64 base64=new Base64();
		boolean flag=false;
		try {
			//注意点：实际的图片数据是从 data:image/jpeg;base64, 后开始的
			byte[] k = base64.decode(file.substring("data:image/jpeg;base64,"
					.length()));
			InputStream is = new ByteArrayInputStream(k);
			String fileName =GetFileName.getFilName(request);
			String imgFilePath = serverPath + File.separator+ fileName + ".jpg";
			//压缩图片
			double ratio = 1.0;
			BufferedImage image = ImageIO.read(is);
			int newWidth = (int) (image.getWidth() * ratio);
			int newHeight = (int) (image.getHeight() * ratio);
			Image newimage = image.getScaledInstance(newWidth, newHeight,
			Image.SCALE_SMOOTH);
			BufferedImage tag = new BufferedImage(newWidth, newHeight,
					BufferedImage.TYPE_INT_RGB);
			Graphics g = tag.getGraphics();
			g.drawImage(newimage, 0, 0, null);
			g.dispose();
			ImageIO.write(tag, "jpg", new File(imgFilePath));
//			String psth="C:\\Users\\13419\\Desktop\\asdf\\"+fileName+".jpg";
//			OutputStream out=new FileOutputStream(new File(psth));
//			out.write(k, 0, k.length);
			PopulationSort populationSort=new PopulationSort();
			populationSort.setSortName(sortName);
			populationSort.setPicture(fileName+".jpg");
			this.populationSortService.doInsertPopulationSort(populationSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	@ResponseBody
	@RequestMapping(value="/getPopulationSortById")
	public PopulationSort getPopulationSortByPopulationSortId(Integer populationId) {
		return this.populationSortService.getPopulationByPopulationId(populationId);
	}
	
	@ResponseBody
	@RequestMapping(value="/doUpdatePopulationSort")
	public boolean doUpdatePopulationSort(HttpServletRequest request, String file,PopulationSort populationSort) {
		String serverPath = request.getSession().getServletContext()
				.getRealPath("/bicycleImg");
		Base64 base64=new Base64();
		boolean flag=false;
		try {
			if(null==file||"".equalsIgnoreCase(file)) {
				this.populationSortService.doUpdatePopulationSort(populationSort);
			}else {
				//注意点：实际的图片数据是从 data:image/jpeg;base64, 后开始的
				byte[] k = base64.decode(file.substring("data:image/jpeg;base64,"
						.length()));
				String fileName =GetFileName.getFilName(request);
				String imgFilePath = serverPath + File.separator+ fileName + ".jpg";
				//压缩图片并保存图片
				OutputStream out=new FileOutputStream(new File(imgFilePath));
				out.write(k, 0, k.length);
				out.close();
				populationSort.setPicture(fileName+".jpg");
				this.populationSortService.doUpdatePopulationSort(populationSort);
			}
			
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	@ResponseBody
	@RequestMapping(value = "/doDeletePopulation")
	public boolean doDeletePopulation(Integer populationId) {
		return this.populationSortService.doDeletePopulationSort(populationId);
	}
}
