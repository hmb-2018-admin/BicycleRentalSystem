package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.RentCar;
import com.service.RentCarService;

@Controller
@RequestMapping(value = "/rentCar")
public class RentCarController {
	@Reference
	private RentCarService rentCarService;
	
	@ResponseBody
	@RequestMapping(value = "/getRentCarByQuery")
	public List<RentCar> getRentCarByQuery(Integer bicycleId ,String startDate ,String expiryDate){
		return rentCarService.getRentCarByBicycleIdAndStartDateAndExpiryDate(bicycleId, startDate, expiryDate);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doReturnCar")
	public boolean doReturnCar(Integer rentId) {
		return this.rentCarService.doReturnCar(rentId);
	}
	
}
