package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.Coupons;
import com.service.CouponsService;

@Controller
@RequestMapping(value = "/coupons")
public class CouponsController {

	@Reference
	private CouponsService couponsService;
	
	@ResponseBody
	@RequestMapping(value = "/getCouponsByQuery")
	public List<Coupons> getCouponsByQuery(String startDate,String expiryDate,Double priceMin ,Double priceMax){
		return this.couponsService.getCouponsBuQuery(startDate, expiryDate, priceMin, priceMax);
	}
	
	@ResponseBody
	@RequestMapping(value = "/doInsertCoupons")
	public boolean doInsertCoupons(Coupons coupons) {
		return this.couponsService.doInsertCoupons(coupons);
	}
	@ResponseBody
	@RequestMapping(value = "/doUpdateCoupons")
	public boolean doUpdateCoupons(Coupons coupons) {
		return this.couponsService.doUpdateCoupons(coupons);
	}
	@ResponseBody
	@RequestMapping(value = "/doDeleteCoupons")
	public boolean doDeleteCoupons(Integer couponsId) {
		return this.couponsService.doDeleteCoupons(couponsId);
	}
	@ResponseBody
	@RequestMapping(value = "/getCouponsByCouponsId")
	public Coupons getCouponsByCouponsId(Integer couponsId) {
		return this.couponsService.getCouponsByCouponsId(couponsId);
	}
}
