package com.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.entities.BicyclePrice;
import com.service.BicyclePriceService;

@Controller
@RequestMapping(value = "/bicyclePrice")
public class BicyclePriceController {
	
	@Reference
	private BicyclePriceService bicyclePriceService;
	
	@ResponseBody
	@RequestMapping(value = "/getBicyclePrice")
	public List<BicyclePrice> getBicyclePrice(String populationIds,String degreeIds ,String timeIds,Double priceMin,Double priceMax) {
		return this.bicyclePriceService.getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(populationIds, degreeIds, timeIds, priceMin, priceMax);
	}
	@ResponseBody
	@RequestMapping(value = "/doInsertBicyclePrice")
	public boolean doInsertBicyclePrice(BicyclePrice bicyclePrice) {
		return this.bicyclePriceService.doInsertBicyclePrice(bicyclePrice);
	}
	@ResponseBody
	@RequestMapping(value = "/doUpdateBicyclePrice")
	public boolean doUpdateBicyclePrice(BicyclePrice bicyclePrice) {
		return this.bicyclePriceService.doUpdateBicyclePrice(bicyclePrice);
	}
	@ResponseBody
	@RequestMapping(value = "/doDeleteBicyclePrice")
	public boolean doDeleteBicyclePrice(Integer priceId) {
		return this.bicyclePriceService.doDeleteBicyclePrice(priceId);
	}
	@ResponseBody
	@RequestMapping(value = "/getBicyclePriceByPriceId")
	public BicyclePrice getBicyclePriceByPriceId(Integer priceId) {
		return this.bicyclePriceService.getBicyclePriceByPriceId(priceId);
	}
	@ResponseBody
	@RequestMapping(value = "/verifyBicyclePrice")
	public Integer verifyBicyclePrice(BicyclePrice bicyclePrice) {
		Integer priceId=null;
		BicyclePrice price=this.bicyclePriceService.getBicyclePriceByBicyclePrice(bicyclePrice);
		if(null != price) {
			priceId=price.getPriceId();
		}
		return priceId;
	}
}
