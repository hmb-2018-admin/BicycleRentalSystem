<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>卡券管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">
        <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
    </head>
    <script type="text/javascript">
   	function showCoupons(){
   		var startDate=$("#start").val();
   		var expiryDate=$("#end").val();
   		var priceMin=$(".priceMin").val();
   		var priceMax=$(".priceMax").val();
   		var param={
   				priceMin:priceMin,
   				priceMax:priceMax,
   				startDate:startDate,
   				expiryDate:expiryDate
   		}
   		$.post("${pageContext.request.contextPath}/coupons/getCouponsByQuery",param,function(data){
   			console.log(data);
   			 var coupons=eval(data);
   			var tr="";
   			for(var i=0 ; i<coupons.length; i++){
   				tr+="<tr><td>"+coupons[i].price+
   				"</td><td>"+coupons[i].astrictPrice+
   				"</td><td>"+coupons[i].number+
   				"</td><td>"+coupons[i].startDate+
   				"</td><td>"+coupons[i].expiryDate+
   				"</td><td><a title='修改'  href ='"+coupons[i].couponsId+"' data-toggle='modal' data-target='#coupons' class='updateCoupons'>"+
                "<i class='iconfont' style='font-size: 22px;'>&#xe69e;</i></a>"+
                "<a title='删除'   href ='"+coupons[i].couponsId+"' class='deleteCoupons'>"+
                "<i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a></td></tr>";
   			}
   			$(".coupons-body").html(tr);
   		});
   	}
    $(function(){
    	
    	showCoupons();
    	
    	//弹出添加框
    	$(document).on("click",".insertCoupons",function(){
    		$(".ensure-coupons").attr("lay-filter","insertForm");
    		$("#avatar-modal-label").html("添加卡券信息");
    		$(".price").val('');
    		$(".astrictPrice").val('');
    		$(".number").val('');
    		$("#startDate").val('');
    		$("#expiryDate").val('');
    	});
    	//弹出修改框
    	$(document).on("click",".updateCoupons",function(){
    		$(".ensure-coupons").attr("lay-filter","updateForm");
    		$("#avatar-modal-label").html("修改价格信息");
    		var couponsId=$(this).attr("href");
    		$.post("${pageContext.request.contextPath}/coupons/getCouponsByCouponsId",{couponsId:couponsId},function(data){
    			var coupons=eval(data);
    			$(".price").val(coupons.price);
        		$(".astrictPrice").val(coupons.astrictPrice);
        		$("#startDate").val(coupons.startDate);
        		$("#expiryDate").val(coupons.expiryDate);
        		$(".couponsId").val(coupons.couponsId);
        		$(".number").val(coupons.number);
    		});
    		return false;
    	});
    	
    	//删除价格
    	$(document).on("click",".deleteCoupons",function(){
    		var couponsId= $(this).attr("href");
    		var con =confirm("您确定要删除这个优惠券吗");
    		if(con==true){
    			$.post("${pageContext.request.contextPath}/coupons/doDeleteCoupons",{couponsId:couponsId},function(data){
        			if(data==true||data=='true'){
        				alert("删除成功");
        				
        			}else{
        				alert("删除失败");
        			}
        			showCoupons();
        		});
    		}
    		return false;
    	});
    });
    </script>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>卡券管理</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="开始日" name="startDate" id="start" type="text" autocomplete="off"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="截止日" name="endDate" id="end" type="text" autocomplete="off"></div>
                                <span>优惠范围:</span>
                                <div class="layui-input-inline" style="width: 100px;">
                                  <input type="number"  placeholder="￥" autocomplete="off" class="layui-input priceMin">
                                </div>
                                <span>-</span>
                                <div class="layui-input-inline" style="width: 100px;">
                                  <input type="number"  placeholder="￥" autocomplete="off" class="layui-input priceMax">
                                </div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="layui-btn" lay-submit lay-filter="sreach">
                                        <i class="layui-icon">&#xe615;</i></button>
                                </div>
                            </form>
                        </div>
                         <div class="layui-card-header">
                            <button class="layui-btn insertCoupons" data-toggle="modal"
							data-target="#coupons">
                                <i class="layui-icon"></i>添加卡券</button></div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>优惠金额</th>
                                        <th>满减金额</th>
                                        <th>发行数量</th>
                                        <th>使用始时间</th>
                                        <th>使用止时间</th>
                                        <th>管理</th>
                                        <!-- <th>管理</th> -->
                                        </tr>
                                </thead>
                                <tbody class="coupons-body">
                                </tbody>
                            </table>
                        </div>
                        <!-- <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <a class="prev" href="">&lt;&lt;</a>
                                    <a class="num" href="">1</a>
                                    <span class="current">2</span>
                                    <a class="num" href="">3</a>
                                    <a class="num" href="">489</a>
                                    <a class="next" href="">&gt;&gt;</a></div>
                            </div>
                        </div> -->
                        <div class="modal fade" id="coupons" aria-hidden="true"
						style="opacity: 1;" aria-labelledby="avatar-modal-label"
						role="dialog" tabindex="-1">
						<div class="modal-dialog modal-lg">
							<div class="modal-content"
								style="width: 700px; margin-top: 100px;">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type="button">&times;</button>
									<h4 class="modal-title" id="avatar-modal-label">添加</h4>
								</div>
								<form class="layui-form" action="">
								<div class="modal-body">
									<div style='margin-left: 200px;' class='content'>
										<span>优惠额度：</span>
										<div class="layui-input-inline"
											style=" margin-left: 14px;">
											<input type="text"  autocomplete="off"
												class="layui-input price" lay-verify="required|number"> 
										</div>
										<br/><br/>
										<span>满减额度：</span>
										<div class="layui-input-inline"
											style=" margin-left: 14px;">
											<input type="text"  autocomplete="off"
												class="layui-input astrictPrice" lay-verify="required|number"> 
										</div>
										<br/><br/>
										<span>发行数量：</span>
										<div class="layui-input-inline"
											style=" margin-left: 14px;">
											<input type="text"  autocomplete="off"
												class="layui-input number" lay-verify="required|number"> 
										</div>
										<br/><br/>
										<span>使用开始时间</span>
										<div class="layui-input-inline layui-show-xs-block">
											<input class="layui-input" placeholder="开始日" name="startDate" 
											id="startDate" type="text" autocomplete="off" lay-verify="required">
										</div>
										<br/><br/>
										<span>使用截止时间</span>
										<div class="layui-input-inline layui-show-xs-block">
											<input class="layui-input" placeholder="截止日" name="expiryDate" 
											id="expiryDate" type="text" autocomplete="off" lay-verify="required">
											<input class='couponsId' type="hidden">
										</div>
										<div class="row">
											<div class="col-md-2">
											<br>
												<button class="btn btn-danger btn-block ensure-coupons"
													type="button" data-dismiss="modal" lay-submit lay-filter
													style="margin-left: 100px;" >确定</button>
											</div>
										</div>
									</div>
									</div>
								</form>
 							   </div>
							</div>
						</div>
					  </div>
                    </div>
                </div>
            </div>
    </body>
    <script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#start' 
                ,trigger: 'click'//指定元素
            });
            //执行一个laydate实例
            laydate.render({
                elem: '#end',
               	trigger: 'click'
            });
            laydate.render({
                elem: '#startDate'
            });
            laydate.render({
                elem: '#expiryDate'
            });
            
            var form = layui.form;
            
            form.on("submit(sreach)",function(data){
            	showCoupons();
            	return false;
            });
            form.on("submit(insertForm)",function(data){
            	var price =$(".price").val();
        		var astrictPrice=$(".astrictPrice").val();
        		var number=$(".number").val();
        		var startDate=$("#startDate").val();
        		var expiryDate=$("#expiryDate").val();
        		var param={
        				price:price,
        				astrictPrice:astrictPrice,
        				number:number,
        				startDate:startDate,
        				expiryDate:expiryDate
        		}
        		$.post("${pageContext.request.contextPath}/coupons/doInsertCoupons",param,function(data){
        			if(data==true||data=='true'){
        				alert("添加成功");
        			}else{
        				alert("添加失败");
        			}
        			showCoupons();
        		});
            	return false;
            });
            form.on("submit(updateForm)",function(data){
            	var price =$(".price").val();
        		var astrictPrice=$(".astrictPrice").val();
        		var number=$(".number").val();
        		var startDate=$("#startDate").val();
        		var expiryDate=$("#expiryDate").val();
        		var couponsId=$(".couponsId").val();
        		var param={
        				couponsId:couponsId,
        				price:price,
        				number:number,
        				astrictPrice:astrictPrice,
        				startDate:startDate,
        				expiryDate:expiryDate
        		}
        		$.post("${pageContext.request.contextPath}/coupons/doUpdateCoupons",param,function(data){
        			if(data==true||data=='true'){
        				alert("修改成功");
        			}else{
        				alert("修改失败");
        			}
        			showCoupons();
        		});
            	return false;
            });
            
        });
        </script>

</html>