<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
  <head>
      <meta charset="UTF-8">
      <title>自行车报修管理</title>
      <meta name="renderer" content="webkit">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
      		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">
       <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/formSelects-v4.css" />
      <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
      		<script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
  </head>
  <!-- 获取下拉框选项 -->
  <script type="text/javascript">
	var PIDS=new Array();//人数ids
	var DIDS=new Array();//程度ids
	var TIDS=new Array();//时间ids
  	$(function(){
  		showPopulation();
  		function showPopulation(){
  			$.post("${pageContext.request.contextPath}/populationSort/getPopulationSortAll",function(data){
  				var populationSorts =eval(data);
  				//以下代码可以在console中运行测试, 结果查看基础示例第一个
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i=0 ;i<populationSorts.length;i++){
  					PIDS[i]=populationSorts[i].populationId;
  					var population={name:populationSorts[i].sortName,value:populationSorts[i].populationId};
  					arropt[i]=population;
  				}
  				formSelects.data('select1', 'local', {arr:arropt});
  				formSelects.data('select-popu', 'local', {arr:arropt});
  			});
  		}
  		showDegreeSort();
  		function showDegreeSort(){
  			$.post("${pageContext.request.contextPath}/degreeSort/getDegreeSortAll",function(data){
  				var degreeSorts=eval(data); 
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i=0 ;i<degreeSorts.length;i++){
  					DIDS[i]=degreeSorts[i].degreeId;
  					var degreeSort={name:degreeSorts[i].degreeName,value:degreeSorts[i].degreeId};
  					arropt[i]=degreeSort;
  				}
  				formSelects.data('select2', 'local', {arr:arropt});
  				formSelects.data('select-degr', 'local', {arr:arropt});
  			});
  		}
  		showTimeSort();
  		function showTimeSort(){
  			$.post("${pageContext.request.contextPath}/timeSort/getTimeSortAll",function(data){
  				var timeSorts=eval(data);
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i= 0; i < timeSorts.length ; i++){
  					TIDS[i]=timeSorts[i].timeId;
  					var timeSort ={name:timeSorts[i].timeName,value:timeSorts[i].timeId};
  					arropt[i]=timeSort;
  				}
  				formSelects.data('select3', 'local', {arr:arropt});
  				formSelects.data('select-time', 'local', {arr:arropt});
  			});
  		}
  	});
  </script>
  <script type="text/javascript">
  	var states='3';
	function showBicycle(){
  		//获取使用人数的值
  		var formSelects = layui.formSelects;
  		var populationIds=formSelects.value('select1','valStr'); 
  		var degreeIds=formSelects.value('select2','valStr'); 
  		var timeIds=formSelects.value('select3','valStr'); 
  		var priceMax=$(".priceMax").val();
  		var priceMin=$(".priceMin").val();
  		var brand =$(".brand").val();
  		var param={
  				degreeIds:degreeIds,
  				timeIds:timeIds,
  				populationIds:populationIds,
  				priceMax:priceMax,
  				priceMin:priceMin,
  				brand:brand,
  				states:states
  				
  		}
  		$.post("${pageContext.request.contextPath}/bicycle/getBicycleByQueryCondition",param,function(data){
		var bicycle=eval(data); 
		var tr="";
		for(var i = 0 ; i < bicycle.length ; i++){
			var state="";
			var str="";
			if(bicycle[i].state==3){
				state="报修";
				str="<button type='button' class='layui-btn layui-btn-radius layui-btn-normal state4' bid='"+bicycle[i].bicycleId+"'>维修</button> <button type='button' class='layui-btn layui-btn-radius layui-btn-warm state1' bid='"+bicycle[i].bicycleId+"'>投入使用</button> ";
			}else{
				state="正在维修";
				str="<button type='button' class='layui-btn layui-btn-radius layui-btn-warm state1' bid='"+bicycle[i].bicycleId+"'>投入使用</button> ";
			}
			tr+="<tr><td>"+bicycle[i].bicycleNumber+
			"</td><td>"+bicycle[i].brand+
			"</td><td>"+bicycle[i].bicyclePrice.populationSort.sortName+
			"</td><td>"+bicycle[i].bicyclePrice.degreeSort.degreeName+
			"</td><td>"+bicycle[i].bicyclePrice.timeSort.timeName+
			"</td><td> ￥"+bicycle[i].bicyclePrice.price+
			"</td><td> "+state+
			"</td><td>"+str+"</td></tr>";
		}
		$(".bicycle-body").html(tr);
  		});
  	}
  $(function(){
	showBicycle();
	
	$(document).on("click",".state4",function(){
		var con= confirm("您确定要进行维修");
    	if (con == true || con=='true') {
    		var bicycleId=$(this).attr("bid");
    		var param={
    				state:4,
    				bicycleId:bicycleId
    		}
    		$.post("${pageContext.request.contextPath}/bicycle/doUpdateStateByBicycleIdAndState",param,function(data){
    			if(data==true||data=='true'){
    				alert("该自行车已提交维修中");
    				
    			}else{
    				alert("该自行车无法维修");
    			}
    			showBicycle();
    		});
    	}else{
    		return false;
    	}
	});
	$(document).on("click",".state1",function(){
		var con= confirm("该自行车是否可以正常使用？");
    	if (con == true || con=='true') {
    		var bicycleId=$(this).attr("bid");
    		var param={
    				state:1,
    				bicycleId:bicycleId
    		}
    		$.post("${pageContext.request.contextPath}/bicycle/doUpdateStateByBicycleIdAndState",param,function(data){
    			if(data==true||data=='true'){
    				alert("该自行车已正常使用中");
    			}else{
    				alert("该自行车无法使用");
    			}
    			showBicycle();
    		});
    	}else{
    		return false;
    	}
	});
  });
  </script>
  <style>
  .content span{
  	font-size: 14px;
    margin-left: 20px;
  }
  </style>
<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">首页</a> <a> <cite>自行车报修管理</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body "  style="padding: 0px;">
						<div class="layui-collapse" lay-filter="test">
							<div class="layui-colla-item">
								<h2 class="layui-colla-title">
									条件筛选<i class="layui-icon layui-colla-icon"></i>
								</h2>
								<div class="layui-colla-content">
									<form class="layui-form" action="">
										<div class="layui-form-item">
											<div class="layui-inline">
												<label class="layui-form-label">价格范围</label>
												<div class="layui-input-inline" style="width: 100px;">
													<input type="text" name="priceMin" placeholder="￥"
														autocomplete="off" class="layui-input priceMin">
												</div>
												<div class="layui-form-mid">-</div>
												<div class="layui-input-inline" style="width: 100px;">
													<input type="text" name="priceMax" placeholder="￥"
														autocomplete="off" class="layui-input priceMax">
												</div>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">乘车人数</label>
											<div class="layui-input-inline">
												<select id="populationSort" xm-select="select1"
													xm-select-skin="normal" xm-select-height="36px" name='populationIds'>
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">使用程度</label>
											<div class="layui-input-inline">
												<select id="degreeSort"  xm-select="select2"
													xm-select-skin="warm" xm-select-height="36px" name='degreeIds'>
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">使用时间</label>
											<div class="layui-input-inline">
												<select xm-select="select3" id="timeSort"
													xm-select-skin="danger" xm-select-height="36px" name='timeIds' >
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<div class="layui-inline">
												<label class="layui-form-label">品牌:</label>
												<div class="layui-input-inline">
													<input type="text" class='brand layui-input' name='brand'>
												</div>
											</div>
										</div>
										<div class="layui-form-item">
											<div class="layui-input-block">
												<button class="serach layui-btn" lay-submit
													lay-filter="serachForm">
													<i class="layui-icon">&#xe615;</i>搜索
												</button>
												<button type="reset" class="layui-btn layui-btn-primary">重置</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="layui-card-header">
						onclick="xadmin.open('添加用户','./order-add.html',800,600)"
						<button class="layui-btn insertPrice" data-toggle="modal"
							data-target="#bicycle">
							<i class="layui-icon"></i>添加
						</button>
					</div> -->
					<div class="layui-tab layui-tab-brief" lay-filter="BicycleState">
					  <ul class="layui-tab-title">
					    <li class="layui-this">报修自行车</li>
					    <li>维修自行车</li>
					  </ul>
					  <div class="layui-tab-content" style="padding: 0px;">
					  	<div class="layui-card-body " style="padding: 0px;">
								<table class="layui-table layui-form">
									<thead>
										<tr>
											<th>自行车编号</th>
											<th>品牌</th>
											<th>乘坐人数</th>
											<th>使用程度</th>
											<th>使用时间</th>
											<th>价格</th>
											<th>状态</th>
											<th>自行车管理</th>
										</tr>
									</thead>
									<tbody class='bicycle-body'>
									</tbody>
								</table>
							</div>
					  </div>
					</div>      
				</div>
			</div>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath }/js/jquery-v3.2.1.js"
	type="text/javascript" charset="utf-8"></script>
<script
	src="${pageContext.request.contextPath }/js/formSelects-v4.min.js"
	type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
layui.use(['laydate', 'form'],
	        function() {
	            var laydate = layui.laydate;

	            //执行一个laydate实例
	            laydate.render({
	                elem: '#start' //指定元素
	            });
	            
	            //执行一个laydate实例
	            laydate.render({
	                elem: '#end' //指定元素
	            });
	            
	            var form = layui.form;
	            
	            //form.render(); 
	            
	            //监听提交
	            form.on('submit(serachForm)', function(data){
	              showBicycle();
	              $(".layui-colla-title").click();
	              return false;
	            });
	            //监听修改状态
	           /*  form.on("switch(state3)",function(data){
	            	if(data.elem.checked){
	            	var con= confirm("您确定要进行维修");
	            	if (con == true) {
	            		$.post("${pageContext.request.contextPath}/bicycle/doUpdateStateByBicycleIdAndState",{state:4});
	            		showBicycle();
	            		alert("提交维修，正在维修中");
	            	} else {
	            		$("input[lay-skin='switch'][value='"+data.value+"']").prop("checked",false);
	            		form.render();
	            	}
	            	}
	            }); */
	        });
	layui.use('element', function(){
	  var element = layui.element;
	  
	  //一些事件监听
	  element.on('tab(BicycleState)', function(data){
	    if(data.index==1){
	    	states="4";
	    }else{
	    	states="3";
	    }
	   	showBicycle();
	  });
	});
</script>
</html>