<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>MerchantManage</title>
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/font.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/xadmin.css">

		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/manage.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">

		<script src="${pageContext.request.contextPath }/js/jquery.min.js"></script>
		<script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
		<script src="${pageContext.request.contextPath }/head/jquery.min.js"></script>

		<link href="${pageContext.request.contextPath }/head/cropper.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/head/sitelogo.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css">

		<script src="${pageContext.request.contextPath }/head/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath }/head/cropper.js"></script>
		<script src="${pageContext.request.contextPath }/head/sitelogo.js"></script>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/nav.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/font-awesome.min.css">
		<style type="text/css">
			body{
				padding:0;
				margin:0;
				background: #f2f2f2;
			}
			.wrap .line{
				height: 500px;
				width: 188px;
				margin:0 auto;
			}
		</style>
		<style type="text/css">
			.avatar-btns button {
				height: 35px;
			}
		</style>
	</head>
	<!-- 人数分类js -->
	<script type="text/javascript">
	$(function(){
		showPopulationSort();
		//显示人数分类
		function showPopulationSort(){
			$.post("${pageContext.request.contextPath}/populationSort/getPopulationSortAll",function(data){
				var populationSorts =eval(data);
				var str="";
				for(var i=0 ;i<populationSorts.length;i++){
					str+="<tr><td>"+populationSorts[i].sortName+"</td>"+
						"<td><img src='${pageContext.request.contextPath}/bicycleImg/"+populationSorts[i].picture+"' width='200px' height='200px'></td>"+
						"<td><button type='button' class='btn btn-primary updPopulationSort' pid='"+populationSorts[i].populationId+"' data-toggle='modal' data-target='#avatar-modal' style='margin: 10px;'>修改分类</button>"+
						"<button type ='button' class ='btn btn-primary delPopulationSort'  pid ='"+populationSorts[i].populationId+"'>删除</button> "+
						"</td></tr>";
				}
				$("#population_tbody").html(str);
				
			});
		}
		$(document).on("click",".delPopulationSort",function(){
			var con=confirm("您确定要删除该类别吗？");
			var populationId=$(this).attr("pid");
			if(con==true){
				$.post("${pageContext.request.contextPath}/populationSort/doDeletePopulation",{populationId:populationId},function(data){
					if(data==true||data=='true'){
						alert("删除成功");
						showPopulationSort();
					}else{
						alert("删除失败！");
					}
				});	
			}
		});
		//弹出修改框
		$(document).on("click",".updPopulationSort",function(){
			$("#avatar-modal-label").html("修改分类信息");
			var populationId=$(this).attr("pid");
			$.post("${pageContext.request.contextPath}/populationSort/getPopulationSortById",{populationId:populationId},function(data){
				$(".sortName").val(data.sortName);
				$(".populationId").val(data.populationId);
				$(".picture").val(data.picture);
				document.querySelector("#avatarInput").outerHTMl=document.querySelector("#avatarInput").outerHTMl;
				$(".avatar-save").removeClass("insertPopulation");
				$(".avatar-save").addClass("updatePopulation");
			});
			
		});
		
		$(document).on("click",".updatePopulation",function(){
			event.defaultPrevented;
			var img_lg = document.getElementById('imageHead');
			var sortName=$(".sortName").val();
			var populationId=$(".populationId").val();
			var picture=$(".picture").val();
			if(sortName==null||sortName==''){
				alert("分类名称不能为空");
				return false;
			}
			// 截图小的显示框内的内容
			html2canvas(img_lg, {
				allowTaint: true,
				taintTest: false,
				onrendered: function(canvas) {
					canvas.id = "mycanvas";
					//生成base64图片数据
					var dataUrl = canvas.toDataURL("image/jpeg");
					var newImg = document.createElement("img");
					if($(".avatar-input").val()==null||$(".avatar-input").val()==''){
						dataUrl=null;
					}
					var param={
							file:dataUrl,
							sortName:sortName,
							populationId:populationId,
							picture:picture
					}
					$.ajax({
						type:"POST",
						url :"${pageContext.request.contextPath}/populationSort/doUpdatePopulationSort",
						data: param,
						success:function(data){
							if(data==true){
								alert("修改成功");
							}else{
								alert("修改失败");
							}
							$(".avatar-save").removeClass("updatePopulation");
							showPopulationSort();
						}
						
					});
				}
			});
		});
		
		//弹出添加框
		$(".insertPicture").click(function(){
			document.querySelector("#avatarInput").outerHTMl=document.querySelector("#avatarInput").outerHTMl;
			$("#avatar-modal-label").html("添加新分类");
			$(".avatar-save").removeClass("updatePopulation");
			$(".avatar-save").addClass("insertPopulation");
			$(".sortName").val('');
		});
		
		$(document).on("click",".insertPopulation",function(event){
			event.defaultPrevented;
			var img_lg = document.getElementById('imageHead');
			var sortName=$(".sortName").val();
			if(sortName==null||sortName==''){
				alert("分类名称不能为空");
				return false;
			}
			if($(".avatar-input").val()==null||$(".avatar-input").val()==''){
				alert("图片不能为空，请选中要上传的图片");
				return false;
			}
			// 截图小的显示框内的内容
			html2canvas(img_lg, {
				allowTaint: true,
				taintTest: false,
				onrendered: function(canvas) {
					canvas.id = "mycanvas";
					//生成base64图片数据
					var dataUrl = canvas.toDataURL("image/jpeg");
					var newImg = document.createElement("img");
					var param={
							file:dataUrl,
							sortName:sortName
					}
					$.ajax({
						type:"POST",
						url :"${pageContext.request.contextPath}/populationSort/doInsertPopulationSort",
						data: param,
						success:function(data){
							if(data==true){
								alert("添加成功");
							}else{
								alert("添加失败");
							}
							$(".avatar-save").removeClass("insertPopulation");
							showPopulationSort();
						}
						
					});
				}
			});
		});
		
	});
	</script>
	<!-- 使用程度分类 js -->
	<script type="text/javascript">
	$(function(){
		showDegreeSort();
		function showDegreeSort(){
			$.post("${pageContext.request.contextPath}/degreeSort/getDegreeSortAll",function(data){
				var degreeSorts=eval(data); 
				var str="";
				for(var i=0 ;i<degreeSorts.length;i++){
					str+="<tr><td>"+degreeSorts[i].degreeName+
					"</td><td><a title='编辑'  href='"+degreeSorts[i].degreeId+"' data-toggle='modal' data-target='#avatar-modal-degree' class='updateDegreeSort'><i class='layui-icon' style='font-size: 22px;'>&#xe642;</i></a>"+
                    "<a title='删除'  href='"+degreeSorts[i].degreeId+"' class='deleteDegree'><i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a>"+
                    "</td></tr>";
				}
				$("#degree_tbody").html(str);
			});
		}
		//弹出添加模态框
		$(document).on("click",".insertDegreeSort",function(){
			$("#avatar-modal-label").html("添加使用程度");
			$(".ensure-degree").removeClass("updDegreeSort");
			$(".ensure-degree").addClass("insDegreeSort");
			$(".degreeName").val('');
		});
		//点击添加
		$(document).on("click",".insDegreeSort",function(){
			var degreeName=$(".degreeName").val();
			$.post("${pageContext.request.contextPath}/degreeSort/doInsertDegreeSort",{degreeName:degreeName},function(data){
				if(data==true||data=='true'){
					alert("添加成功");
					showDegreeSort();
				}else{
					alert("添加失败");
				}
			});
		});
		var degreeId;
		//弹出修改框
		$(document).on("click",".updateDegreeSort",function(event){
			event.preventDefault();
			degreeId = $(this).attr("href");
			$.post("${pageContext.request.contextPath}/degreeSort/getDegreeSortByDegreeId",{degreeId:degreeId},function(data){
				$(".ensure-degree").addClass("updDegreeSort");
				$(".ensure-degree").removeClass("insDegreeSort");
				$("#avatar-modal-label").html("修改使用程度");
				$(".degreeName").val(data.degreeName);
			});
			return false;
		});
		
		//确定修改
		$(document).on("click",".updDegreeSort",function(){
			var degreeName=$(".degreeName").val();
			var param={
					degreeName:degreeName,
					degreeId:degreeId
			}
			$.post("${pageContext.request.contextPath}/degreeSort/doUpdateDegreeSort",param,function(data){
				if(data==true||data=='true'){
					alert("修改成功");
					showDegreeSort();
				}else{
					alert("修改失败");
				}
			});
		});
		
		//删除分类
		$(document).on("click",".deleteDegree",function(event){
			event.preventDefault(); 
			var con=confirm("您确定要删除该类别吗？");
			var degreeId=$(this).attr("href");
			if(con==true){
				$.post("${pageContext.request.contextPath}/degreeSort/doDeleteDegreeSort",{degreeId:degreeId},function(data){
					if(data==true||data=='true'){
						alert("删除成功");
						showDegreeSort();
					}else{
						alert("删除失败！");
					}
				});	
			}
			return false;
		});
		
	});
	</script>
	<!-- 使用时间分类js  -->
	<script type="text/javascript">
	$(function(){
		showTimeSort();
		function showTimeSort(){
			$.post("${pageContext.request.contextPath}/timeSort/getTimeSortAll",function(data){
				var timeSorts=eval(data);
				var str="";
				for(var i=0 ;i<timeSorts.length;i++){
					str+="<tr><td>"+timeSorts[i].timeName+
					"</td><td><a title='编辑'  href='"+timeSorts[i].timeId+"' data-toggle='modal' data-target='#avatar-modal-time' class='updateTimeSort'><i class='layui-icon' style='font-size: 22px;'>&#xe642;</i></a>"+
                    "<a title='删除'  href='"+timeSorts[i].timeId+"' class='deleteTime'><i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a>"+
                    "</td></tr>";
				}
				$("#time_tbody").html(str);
			});
		}
		
		//弹出添加模态框
		$(document).on("click",".insertTimeSort",function(){
			$("#avatar-modal-label").html("添加使用时间");
			$(".ensure-time").removeClass("updTimeSort");
			$(".ensure-time").addClass("insTimeSort");
			$(".timeName").val('');
		});
		//点击添加
		$(document).on("click",".insTimeSort",function(){
			var timeName=$(".timeName").val();
			$.post("${pageContext.request.contextPath}/timeSort/doInsertTimeSort",{timeName:timeName},function(data){
				if(data==true||data=='true'){
					alert("添加成功");
					showTimeSort();
				}else{
					alert("添加失败");
				}
			});
		});
		var timeId;
		//弹出修改框
		$(document).on("click",".updateTimeSort",function(event){
			event.preventDefault(); 
			timeId = $(this).attr("href");
			$.post("${pageContext.request.contextPath}/timeSort/getTimeSortByTimeId",{timeId:timeId},function(data){
				$(".ensure-time").addClass("updTimeSort");
				$(".ensure-time").removeClass("insTimeSort");
				$(".timeName").val(data.timeName);
				$("#avatar-modal-label").html("修改使用时间");
			});
			return false;
		});
		
		//确定修改
		$(document).on("click",".updTimeSort",function(){
			var timeName=$(".timeName").val();
			var param={
					timeName:timeName,
					timeId:timeId
			}
			$.post("${pageContext.request.contextPath}/timeSort/doUpdateTimeSort",param,function(data){
				if(data==true||data=='true'){
					alert("修改成功");
					showTimeSort();
				}else{
					alert("修改失败");
				}
			});
		});
		
		//删除分类
		$(document).on("click",".deleteTime",function(event){
			event.preventDefault(); 
			var con=confirm("您确定要删除该类别吗？");
			var timeId=$(this).attr("href");
			if(con==true){
				$.post("${pageContext.request.contextPath}/timeSort/doDeleteTime",{timeId:timeId},function(data){
					if(data==true||data=='true'){
						alert("删除成功");
						showTimeSort();
					}else{
						alert("删除失败！");
					}
				});	
			}
			return false;
		});
		
	});
	</script>
	<style>
	p {	display: inline-block;}
	</style>
	<body style="background: #f1f1f1;">
	<div class="leftNav-item">
		<ul>
			
			<li title="人数管理"><i class="fa fa-bicycle"></i> <a
				href="#populationManage" class="rota">人数管理</a></li>
			<li title="程度管理"><i class="fa fa-hourglass-half"></i> <a
				href="#degreeManage" class="rota">程度管理</a></li>
			<li title="时间管理"><i class="fa fa-clock-o"></i> <a
				href="#timeManage" class="rota">时间管理</a></li>
			<li title="返回顶部" class="for-top"><i class="fa fa-arrow-up"></i>
				<a href="javascript:;" class="rota">去顶部</a></li>
		</ul>
	</div>
	<br>
	<!-- 人数管理 -->
	<div class="box" style="width: 1000px">
		<div class="title" id="populationManage">自行车人数分类</div>
		<div class="content">
			<div>
				<br>
				<button type="button" class="btn btn-primary insertPicture"
					data-toggle="modal" data-target="#avatar-modal"
					style="margin: 10px;">添加分类</button>
				<br>
				<hr>
			</div>
		</div>
		<!--表格列表-->

		<table id="tb" class="table">
			<thead>
				<tr>
					<th width='100px'>分类名称</th>
					<th>图片</th>
					<th width='200px'>管理</th>
				</tr>
			</thead>
			<tbody id="population_tbody">
				<!-- 从数据库获取的值 -->
			</tbody>
		</table>
		<div class="modal fade" id="avatar-modal" aria-hidden="true"
			aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<!--<form class="avatar-form" action="upload-logo.php" enctype="multipart/form-data" method="post">-->
					<form class="avatar-form" enctype="multipart/form-data"
						method="post">
						<div class="modal-header">
							<button class="close" data-dismiss="modal" type="button">&times;</button>
							<h4 class="modal-title" id="avatar-modal-label">修改分类信息</h4>
						</div>
						<div class="modal-body">
							<div class="avatar-body">
								<div class="avatar-upload">
									<label style="line-height: 35px;">分类名称</label> <input
										type="text" name="sortName" class="sortName"> <input
										class="populationId" type="hidden"> <input
										class="picture" type="hidden"> <input
										class="avatar-src" name="avatar_src" type="hidden"> <input
										class="avatar-data" name="avatar_data" type="hidden">
									<label for="avatarInput" style="line-height: 35px;">图片上传</label>
									<button class="btn btn-danger" type="button"
										style="height: 35px; margin-top: 14px;"
										onClick="$('input[id=avatarInput]').click();">请选择图片</button>
									<span id="avatar-name"></span> <input class="avatar-input hide"
										id="avatarInput" name="avatar_file" type="file">
								</div>
								<div class="row">
									<div class="col-md-9">
										<div class="avatar-wrapper"></div>
									</div>
									<div class="col-md-3">
										<div class="avatar-preview preview-lg" id="imageHead"></div>
										<!--<div class="avatar-preview preview-md"></div>
							<div class="avatar-preview preview-sm"></div>-->
									</div>
								</div>
								<div class="row avatar-btns">
									<div class="col-md-4">
										<div class="btn-group">
											<button class="btn btn-danger fa fa-undo"
												data-method="rotate" data-option="-90" type="button"
												title="Rotate -90 degrees">向左旋转</button>
										</div>
										<div class="btn-group">
											<button class="btn  btn-danger fa fa-repeat"
												data-method="rotate" data-option="90" type="button"
												title="Rotate 90 degrees">向右旋转</button>
										</div>
									</div>
									<div class="col-md-5" style="text-align: right;">
										<button class="btn btn-danger fa fa-arrows"
											data-method="setDragMode" data-option="move" type="button"
											title="移动">
											<span class="docs-tooltip" data-toggle="tooltip" title=""
												data-original-title="$().cropper(&quot;setDragMode&quot;, &quot;move&quot;)">
											</span>
										</button>
										<button type="button" class="btn btn-danger fa fa-search-plus"
											data-method="zoom" data-option="0.1" title="放大图片">
											<span class="docs-tooltip" data-toggle="tooltip" title=""
												data-original-title="$().cropper(&quot;zoom&quot;, 0.1)">
												<!--<span class="fa fa-search-plus"></span>-->
											</span>
										</button>
										<button type="button"
											class="btn btn-danger fa fa-search-minus" data-method="zoom"
											data-option="-0.1" title="缩小图片">
											<span class="docs-tooltip" data-toggle="tooltip" title=""
												data-original-title="$().cropper(&quot;zoom&quot;, -0.1)">
												<!--<span class="fa fa-search-minus"></span>-->
											</span>
										</button>
										<button type="button" class="btn btn-danger fa fa-refresh"
											data-method="reset" title="重置图片">
											<span class="docs-tooltip" data-toggle="tooltip" title=""
												data-original-title="$().cropper(&quot;reset&quot;)"
												aria-describedby="tooltip866214"></span>
										</button>
									</div>
									<div class="col-md-3">
										<button
											class="btn btn-danger btn-block avatar-save fa fa-save "
											type="button" data-dismiss="modal">保存修改</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<br>
	<!-- 使用程度管理 -->
	<div class="box" style="width: 1000px">
		<div class="title" id="degreeManage">自行车新旧程度分类</div>
		<div class="content">
			<!--搜索输入框及查询、重置按钮-->
			<div>
				<br>
				<button type="button" class="btn btn-primary insertDegreeSort"
					data-toggle="modal" data-target="#avatar-modal-degree"
					style="margin: 10px;">添加使用程度</button>
				<br>
				<hr>
			</div>
			<!--添加按钮及bootstrap的模态框-->
			<div class="modal fade" id="avatar-modal-degree" aria-hidden="true"
				aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
				<div class="modal-dialog modal-lg">
					<div class="modal-content"
						style="margin-left: 170px; width: 400px; margin-top: 100px;">
						<!--<form class="avatar-form" action="upload-logo.php" enctype="multipart/form-data" method="post">-->
						<form class="avatar-form" enctype="multipart/form-data"
							method="post">
							<div class="modal-header">
								<button class="close" data-dismiss="modal" type="button">&times;</button>
								<h4 class="modal-title" id="avatar-modal-label">修改分类信息</h4>
							</div>
							<div class="modal-body">
								<div>
									<div class="avatar-upload">
										<label style="line-height: 35px; margin-left: 50px;">程度名称</label>
										<input type="text" class="degreeName" style="margin-top: 6px;">
									</div>
									<div class="row">
										<div class="col-md-10">
											<button class="btn btn-danger btn-block ensure-degree" type="button"
												data-dismiss="modal" style="margin-left: 36px;">确定</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--表格列表-->
			<table id="tb" class="table">
				<thead>
					<tr>
						<th>使用程度</th>
						<th>分类管理</th>
					</tr>
				</thead>
				<tbody id="degree_tbody">
					<!-- 从数据库获取的值 -->
					<tr>
						<td>全新</td>
						<td><a title="编辑"
							onclick="xadmin.open('编辑','member-edit.html',600,400)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i>
						</a> <a title="删除" onclick="member_del(this,'要删除的id')"
							href="javascript:;"> <i class="layui-icon">&#xe640;</i>
						</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<!-- 时间管理  -->
	<div class="box" style="width: 1000px">
		<div class="title" id="timeManage">自行车租用时间分类</div>
		<div class="content">
			<!--搜索输入框及查询、重置按钮-->
			<div>
				<br>
				<button type="button" class="btn btn-primary insertTimeSort"
					data-toggle="modal" data-target="#avatar-modal-time"
					style="margin: 10px;">添加租用时间</button>
				<br>
				<hr>
			</div>
			<!--添加按钮及bootstrap的模态框-->
			<div class="modal fade" id="avatar-modal-time" aria-hidden="true"
				aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
				<div class="modal-dialog modal-lg">
					<div class="modal-content"
						style="margin-left: 170px; width: 400px; margin-top: 100px;">
						<!--<form class="avatar-form" action="upload-logo.php" enctype="multipart/form-data" method="post">-->
						<form class="avatar-form" enctype="multipart/form-data"
							method="post">
							<div class="modal-header">
								<button class="close" data-dismiss="modal" type="button">&times;</button>
								<h4 class="modal-title" id="avatar-modal-label">修改分类信息</h4>
							</div>
							<div class="modal-body">
								<div>
									<div class="avatar-upload">
										<label style="line-height: 35px; margin-left: 50px;">租用时间</label>
										<input type="text" class="timeName" style="margin-top: 6px;">
									</div>
									<div class="row">
										<div class="col-md-10">
											<button class="btn btn-danger btn-block ensure-time" type="button"
												data-dismiss="modal" style="margin-left: 36px;">确定</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--表格列表-->

			<table id="tb" class="table">
				<thead>
					<tr>
						<th>租用时间</th>
						<th>分类管理</th>
					</tr>
				</thead>
				<tbody id="time_tbody">
					<!-- 从数据库获取的值 -->
					<tr>
						<td>1小时</td>
						<td><a title="编辑"
							onclick="xadmin.open('编辑','member-edit.html',600,400)"
							href="javascript:;"> <i class="layui-icon">&#xe642;</i>
						</a> <a title="删除" onclick="member_del(this,'要删除的id')"
							href="javascript:;"> <i class="layui-icon">&#xe640;</i>
						</a></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<script src="${pageContext.request.contextPath }/js/mejs.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/js/nav.js"></script>
		<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
		<script
			src="${pageContext.request.contextPath }/head/html2canvas.min.js"
			type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			//做个下简易的验证  大小 格式 
			$('#avatarInput').on('change', function(e) {
				var filemaxsize = 1024 * 5; //5M
				var target = $(e.target);
				var Size = target[0].files[0].size / 1024;
				if (Size > filemaxsize) {
					alert('图片过大，请重新选择!');
					$(".avatar-wrapper").childre().remove;
					return false;
				}
				if (!this.files[0].type.match(/image.*/)) {
					alert('请选择正确的图片!')
				} else {
					var filename = document.querySelector("#avatar-name");
					var texts = document.querySelector("#avatarInput").value;
					var teststr = texts; //你这里的路径写错了
					testend = teststr.match(/[^\\]+\.[^\(]+/i); //直接完整文件名的
					filename.innerHTML = testend;
				}
	
			});
	
			/* $(".avatar-save").on("click", function() {
				var img_lg = document.getElementById('imageHead');
				// 截图小的显示框内的内容
				html2canvas(img_lg, {
					allowTaint: true,
					taintTest: false,
					onrendered: function(canvas) {
						canvas.id = "mycanvas";
						//生成base64图片数据
						var dataUrl = canvas.toDataURL("image/jpeg");
						var newImg = document.createElement("img");
						newImg.src = dataUrl;
						imagesAjax(dataUrl)
					}
				});
			})
	
			function imagesAjax(src) {
				var data = {};
				data.img = src;
				data.jid = $('#jid').val();
				$.ajax({
					url: "upload-logo.php",
					data: data,
					type: "POST",
					dataType: 'json',
					success: function(re) {
						if (re.status == '1') {
							$('.user_pic img').attr('src', src);
						}
					}
				}); 
			}*/
		</script>
	</body>
</html>
