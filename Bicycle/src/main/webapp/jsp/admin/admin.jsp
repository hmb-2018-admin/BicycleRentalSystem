<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>管理员管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">
        <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
    </head>
    <script type="text/javascript">
   	function showAdmin(){
   		var adminName=$("#adminName").val();
   		$.post("${pageContext.request.contextPath}/admin/getAdminByAdminNameLike",{adminName:adminName},function(data){
   			console.log(data);
   			 var admin=eval(data);
   			var tr="";
   			for(var i=0 ; i<admin.length; i++){
   				tr+="<tr><td>"+admin[i].adminName+
   				"</td><td><input type='password' value='"+admin[i].adminPassword+"'disabled='disabled' class='layui-input' >"+
   				"</td><td>"+admin[i].realName+
   				"</td><td><a title='修改'  href ='"+admin[i].adminId+"' data-toggle='modal' data-target='#admin' class='updateAdmin'>"+
                "<i class='iconfont' style='font-size: 22px;'>&#xe69e;</i></a>"+
                "<a title='删除'   href ='"+admin[i].adminId+"' class='deleteAdmin'>"+
                "<i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a>"+
                "<a title='重置密码'   href ='"+admin[i].adminId+"' class='updatePassword'>"+
                "<i class='layui-icon' style='font-size: 22px;'>&#xe716;</i></a></td></tr>";
   			}
   			$(".admin-body").html(tr);
   		});
   	}
    $(function(){
    	
    	showAdmin();
    	
    	//弹出添加框
    	$(document).on("click",".insertAdmin",function(){
    		$(".ensure-coupons").attr("lay-filter","insertForm");
    		$("#avatar-modal-label").html("添加新管理员");
    		$(".div-password").css({"display":"none"});
    		$(".adminName").val('');
    		$(".realName").val('');
    	});
    	//弹出修改框
    	$(document).on("click",".updateAdmin",function(){
    		$(".ensure-coupons").attr("lay-filter","updateForm");
    		$("#avatar-modal-label").html("修改管理员信息");
    		$(".div-password").css({"display":"block"});
    		var adminId=$(this).attr("href");
    		$.post("${pageContext.request.contextPath}/admin/getAdminByAdminId",{adminId:adminId},function(data){
    			var admin=eval(data);
    			$(".adminName").val(admin.adminName);
        		$(".realName").val(admin.realName);
        		$(".adminId").val(admin.adminId);
        		$(".adminPasswordOld").attr("aName",admin.adminName);
        		$(".adminPasswordOld").val('');
        		$(".adminPasswordNew").val('');
    		});
    		return false;
    	});
    	
    	//删除价格
    	$(document).on("click",".deleteAdmin",function(){
    		var adminId= $(this).attr("href");
    		var con =confirm("您确定要删除这个管理员吗");
    		if(con==true){
    			$.post("${pageContext.request.contextPath}/admin/doDeleteAdmin",{adminId:adminId},function(data){
        			if(data==true||data=='true'){
        				alert("删除成功");
        				
        			}else{
        				alert("删除失败");
        			}
        			showAdmin();
        		});
    		}
    		return false;
    	});
    	//重置密码
    	$(document).on("click",".updatePassword",function(){
    		var adminId=$(this).attr("href");
    		var con =confirm("您确定要重置密码吗");
    		if(con==true){
	    		$.post("${pageContext.request.contextPath}/admin/doResetPassword",{adminId:adminId},function(data){
	    			if(data==true||data=='true'){
	    				alert("重置成功");
	    			}else{
	    				alert("重置失败");
	    			}
	    			showAdmin();
	    		});
    		}
    		return false;
    	});
    });
    </script>
<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">首页</a> <a> <cite>卡券管理</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<form class="layui-form layui-col-space5">
							<div class="layui-input-inline">
								<input type="text" placeholder="用户名" autocomplete="off"
									class="layui-input " id="adminName">
							</div>
							<div class="layui-input-inline layui-show-xs-block">
								<button class="layui-btn" lay-submit lay-filter="sreach">
									<i class="layui-icon">&#xe615;</i>
								</button>
							</div>
						</form>
					</div>
					<div class="layui-card-header">
						<button class="layui-btn insertAdmin" data-toggle="modal"
							data-target="#admin">
							<i class="layui-icon"></i>添加新管理员
						</button>
					</div>
					<div class="layui-card-body ">
						<table class="layui-table layui-form">
							<thead>
								<tr>
									<th>用户名</th>
									<th style='width:300px'>密码</th>
									<th>真实姓名</th>
									<th>管理</th>
									<!-- <th>管理</th> -->
								</tr>
							</thead>
							<tbody class="admin-body">
							</tbody>
						</table>
					</div>
					<!-- <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <a class="prev" href="">&lt;&lt;</a>
                                    <a class="num" href="">1</a>
                                    <span class="current">2</span>
                                    <a class="num" href="">3</a>
                                    <a class="num" href="">489</a>
                                    <a class="next" href="">&gt;&gt;</a></div>
                            </div>
                        </div> -->
					<div class="modal fade" id="admin" aria-hidden="true"
						style="opacity: 1;" aria-labelledby="avatar-modal-label"
						role="dialog" tabindex="-1">
						<div class="modal-dialog modal-lg">
							<div class="modal-content"
								style="width: 700px; margin-top: 100px;">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type="button">&times;</button>
									<h4 class="modal-title" id="avatar-modal-label">添加</h4>
								</div>
								<form class="layui-form" action="">
									<div class="modal-body">
										<div style='margin-left: 200px;' class='content'>
											<span>用户名：</span>
											<div class="layui-input-inline" style="margin-left: 14px;">
												<input type="text" autocomplete="off"
													class="layui-input adminName" lay-verify="required">
											</div>
											<div class='div-password' style='display:none'><br />
											<br /> <span>原密码：</span>
											<div class="layui-input-inline" style="margin-left: 14px;">
												<input type="password" autocomplete="off" aName 
													class="layui-input adminPasswordOld" lay-verify='password'
													>
											</div>
											<br />
											<br /> <span>新密码：</span>
											<div class="layui-input-inline" style="margin-left: 14px;">
												<input type="password" autocomplete="off"
													class="layui-input adminPasswordNew"
													>
													<input type="hidden" class='adminId'>
											</div>
											</div>
											<br />
											<br /> <span>真实姓名：</span>
											<div class="layui-input-inline" style="margin-left: 14px;">
												<input type="text" autocomplete="off"
													class="layui-input realName"
													lay-verify="required">
											</div>
											<br />
											<div class="row">
												<div class="col-md-2">
													<br>
													<button class="btn btn-danger btn-block ensure-coupons"
														type="button" data-dismiss="modal" lay-submit lay-filter
														style="margin-left: 100px;">确定</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	layui
			.use(
					[ 'laydate', 'form' ],
					function() {
						var laydate = layui.laydate;

						//执行一个laydate实例
						laydate.render({
							elem : '#start',
							trigger : 'click'//指定元素
						});
						//执行一个laydate实例
						laydate.render({
							elem : '#end',
							trigger : 'click'
						});
						laydate.render({
							elem : '#startDate'
						});
						laydate.render({
							elem : '#expiryDate'
						});

						var form = layui.form;

						form.on("submit(sreach)", function(data) {
							showAdmin();
							return false;
						});
						form.verify({
							  password: function(value, item ){ //value：表单的值、item：表单的DOM对象
								  var adminPassword =value;
								  var adminName= item.getAttribute("aName");
								  var param={
										  adminName:adminName,
										  adminPassword:adminPassword
								  }
								  var flag;
								  $.ajaxSetup({ async: false });
								  $.post("${pageContext.request.contextPath}/admin/verifyPassword",param,function(data){
									  flag=data;
								  });
								  if(!flag){
									  return "原始密码输入错误"
								  }
							  }
							});      
						form
								.on(
										"submit(insertForm)",
										function(data) {
											var adminName=$(".adminName").val();
								    		var realName=$(".realName").val();
								    		var param={
								    				adminName:adminName,
								    				realName:realName
								    		}
								    		$.post("${pageContext.request.contextPath}/admin/doInsertAdmin",param,function(data){
								    			if(data==true||data=='true'){
								    				alert("添加成功");
								    			}else{
								    				alert("添加失败");
								    			}
								    			showAdmin();
								    		});
											return false;
										});
						form
								.on(
										"submit(updateForm)",
										function(data) {
											var adminName=$(".adminName").val();
							        		var realName=$(".realName").val();
							        		var adminId=$(".adminId").val();
							        		var oldPassword=$(".adminPasswordOld").val();
							        		var newPassword=$(".adminPasswordNew").val().trim();
							        		var adminPassword=oldPassword;
							        		if(newPassword !='' && newPassword !=null){
							        			adminPassword=newPassword;
							        		}
							        		var param={
							        				adminName:adminName,
							        				realName:realName,
							        				adminId:adminId,
							        				adminPassword:adminPassword
							        		}
							        		console.log(param);
							        		$.post("${pageContext.request.contextPath}/admin/doUpdateAdmin",param,function(data){
							        			if(data==true||data=='true'){
							        				alert("修改成功");
							        			}else{
							        				alert("修改失败");
							        			}
							        			showAdmin();
							        		});
											return false;
										});
					});
	
</script>
</html>