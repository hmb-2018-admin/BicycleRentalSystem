<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
    <head>
        <meta charset="UTF-8">
        <title>价格管理</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
        		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">
         <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/formSelects-v4.css" />
        <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
    </head>
    <!-- 获取下拉框选项 -->
    <script type="text/javascript">
   		var PIDS=new Array();//人数ids
   		var DIDS=new Array();//程度ids
   		var TIDS=new Array();//时间ids
    	$(function(){
    		showPopulation();
    		function showPopulation(){
    			$.post("${pageContext.request.contextPath}/populationSort/getPopulationSortAll",function(data){
    				var populationSorts =eval(data);
    				//以下代码可以在console中运行测试, 结果查看基础示例第一个
    				var formSelects = layui.formSelects;
    				var arropt=new Array();
    				for(var i=0 ;i<populationSorts.length;i++){
    					PIDS[i]=populationSorts[i].populationId;
    					var population={name:populationSorts[i].sortName,value:populationSorts[i].populationId};
    					arropt[i]=population;
    				}
    				formSelects.data('select1', 'local', {arr:arropt});
    				formSelects.data('select-popu', 'local', {arr:arropt});
    				var option="";
    				for(var i=0 ;i<populationSorts.length;i++){
    					option+="<option value='"+populationSorts[i].populationId+"'>"+populationSorts[i].sortName+"</option>";
    				}
    				$("#populationSort").html(option);
    				$("#populationSort1").html(option);
    			});
    		}
    		showDegreeSort();
    		function showDegreeSort(){
    			$.post("${pageContext.request.contextPath}/degreeSort/getDegreeSortAll",function(data){
    				var degreeSorts=eval(data); 
    				/* var option="<option value=''>使用程度</option>";
    				for(var i=0 ;i<degreeSorts.length;i++){
    					option+="<option value='"+degreeSorts[i].degreeId+"'>"+degreeSorts[i].degreeName+"</option>";
    				}
    				$("#degreeSort").html(option); */
    				var formSelects = layui.formSelects;
    				var arropt=new Array();
    				for(var i=0 ;i<degreeSorts.length;i++){
    					DIDS[i]=degreeSorts[i].degreeId;
    					var degreeSort={name:degreeSorts[i].degreeName,value:degreeSorts[i].degreeId};
    					arropt[i]=degreeSort;
    				}
    				formSelects.data('select2', 'local', {arr:arropt});
    				formSelects.data('select-degr', 'local', {arr:arropt});
    			});
    		}
    		showTimeSort();
    		function showTimeSort(){
    			$.post("${pageContext.request.contextPath}/timeSort/getTimeSortAll",function(data){
    				var timeSorts=eval(data);
    				var formSelects = layui.formSelects;
    				var arropt=new Array();
    				for(var i= 0; i < timeSorts.length ; i++){
    					TIDS[i]=timeSorts[i].timeId;
    					var timeSort ={name:timeSorts[i].timeName,value:timeSorts[i].timeId};
    					arropt[i]=timeSort;
    				}
    				formSelects.data('select3', 'local', {arr:arropt});
    				formSelects.data('select-time', 'local', {arr:arropt});
    				/* var option="<option value=''>使用时间</option>";
    				for(var i=0 ;i<timeSorts.length;i++){
    					option+="<option value='"+timeSorts[i].timeId+"'>"+timeSorts[i].timeName+"</option>";
    				}
    				$("#timeSort").html(option); */
    				/* layui.use(['laydate', 'form'],
    				        function() {
    				            var laydate = layui.laydate;

    				            //执行一个laydate实例
    				            laydate.render({
    				                elem: '#start' //指定元素
    				            });

    				            //执行一个laydate实例
    				            laydate.render({
    				                elem: '#end' //指定元素
    				            });
    				        }); */
    			});
    		}
    	});
    </script>
    <script type="text/javascript">
    $(function(){
    	showPrice();
    	function showPrice(){
    		//获取使用人数的值
    		var formSelects = layui.formSelects;
    		var populationIds=formSelects.value('select1','valStr'); 
    		var degreeIds=formSelects.value('select2','valStr'); 
    		var timeIds=formSelects.value('select3','valStr'); 
    		var priceMax=$(".priceMax").val();
    		var priceMin=$(".priceMin").val();
    		var param={
    				degreeIds:degreeIds,
    				timeIds:timeIds,
    				populationIds:populationIds,
    				priceMax:priceMax,
    				priceMin:priceMin
    		}
    		$.post("${pageContext.request.contextPath}/bicyclePrice/getBicyclePrice",param,function(data){
				var bicyclePrices=eval(data); 
				var tr="";
				for(var i = 0 ; i < bicyclePrices.length ; i++){
					tr+="<tr><td>"+bicyclePrices[i].populationSort.sortName+
					"</td><td>"+bicyclePrices[i].degreeSort.degreeName+
					"</td><td>"+bicyclePrices[i].timeSort.timeName+
					"</td><td> ￥"+bicyclePrices[i].price+
					"</td><td><a title='修改'  href ='"+bicyclePrices[i].priceId+"'  data-toggle='modal' data-target='#bicycle-price' class='updatePrice'>"+
                    "<i class='iconfont' style='font-size: 22px;'>&#xe69e;</i></a>"+
                    "<a title='删除'   href ='"+bicyclePrices[i].priceId+"' class='deletePrice'>"+
                    "<i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a></td></tr>";
				}
				$(".price-body").html(tr);
    		});
    	}
    	//查询
    	$(".serach").click(function(){
    		showPrice();
    		return false;
    	});
    	
    	//弹出添加框
    	$(document).on("click",".insertPrice",function(){
    		$(".ensure-price").addClass("insert-price");
    		$(".ensure-price").removeClass("update-price");
    		$("#avatar-modal-label").html("添加价格信息");
    		var formSelects = layui.formSelects;
    		formSelects.undisabled('select-popu');
    		formSelects.undisabled('select-degr');
    		formSelects.undisabled('select-time');
    		formSelects.value('select-popu', PIDS, false);
    		formSelects.value('select-degr', DIDS, false);
    		formSelects.value('select-time', TIDS, false);
    		$(".price").val('');
    	});
    	//确定添加
    	$(document).on("click",".insert-price",function(){
    		var formSelects = layui.formSelects;
    		var populationId=formSelects.value('select-popu','valStr'); 
    		var degreeId=formSelects.value('select-degr','valStr'); 
    		var timeId=formSelects.value('select-time','valStr');
    		var price = $(".price").val();
    		if(populationId==null||populationId==''){
    			alert("乘坐人数不能为空");
    			return false;
    		}
    		if(degreeId==null||degreeId==''){
    			alert("使用程度不能为空");
    			return false;
    		}
    		if(timeId==null||timeId==''){
    			alert("使用时间不能为空");
    			return false;
    		}
    		var reg=/^[0-9]+.?[0-9]*$/; 
    		if(!reg.test(price)){
    			alert("价格请输入数字");
    			return false;
    		}
    		var param={
    				populationId:populationId,
    				degreeId:degreeId,
    				timeId:timeId,
    				price:price
    		}
    		$.post("${pageContext.request.contextPath}/bicyclePrice/verifyBicyclePrice",param,function(data){
    			console.log(data);
    			if(data ==null|| data ==''){
    				$.post("${pageContext.request.contextPath}/bicyclePrice/doInsertBicyclePrice",param,function(date){
    					if(date==true||date=='true'){
    						alert("添加成功");
    						formSelects.value('select1', PIDS, false);
    	    	    		formSelects.value('select2', DIDS, false);
    	    	    		formSelects.value('select3', TIDS, false);
    	    	    		$(".priceMax").val('');
    	    	    		$(".priceMin").val('');
    						showPrice();
    					}else{
    						alert("添加失败");
    					}
    				})
    			}else{
    				alert("该组合的价格已经存在，请重新设置价格");
    				$(".price").val('');
    				formSelects.value('select-popu', PIDS, false);
    	    		formSelects.value('select-degr', DIDS, false);
    	    		formSelects.value('select-time', TIDS, false);
    			}
    		});
    	});
    	//弹出修改框
    	$(document).on("click",".updatePrice",function(){
    		$("#avatar-modal-label").html("修改价格信息");
    		$(".ensure-price").addClass("update-price");
    		$(".ensure-price").removeClass("insert-price");
    		var priceId=$(this).attr("href");
    		var formSelects = layui.formSelects;
    		$.post("${pageContext.request.contextPath}/bicyclePrice/getBicyclePriceByPriceId",{priceId:priceId},function(data){
    			var bicyclePrice=eval(data);
    			$(".priceId").val(bicyclePrice.priceId);
    			formSelects.value('select-popu', [bicyclePrice.populationId], true);
	    		formSelects.value('select-degr', [bicyclePrice.degreeId], true);
	    		formSelects.value('select-time', [bicyclePrice.timeId], true);
	    		formSelects.disabled('select-popu');
	    		formSelects.disabled('select-degr');
	    		formSelects.disabled('select-time');
	    		$(".price").val(bicyclePrice.price);
    		});
    		return false;
    	});
    	
    	//确定修改
    	$(document).on("click",".update-price",function(){
    		var price=$(".price").val();
    		var priceId=$(".priceId").val();
    		var reg=/^[0-9]+.?[0-9]*$/; 
    		if(!reg.test(price)){
    			alert("价格请输入数字");
    			return false;
    		}
    		var param={
    				price:price,
    				priceId:priceId
    		}
    		$.post("${pageContext.request.contextPath}/bicyclePrice/doUpdateBicyclePrice",param,function(data){
    			if(data==true||data=='true'){
    				alert("修改成功");
    				showPrice();
    			}else{
    				alert("修改失败");
    			}
    		});
    	});
    	//删除价格
    	$(document).on("click",".deletePrice",function(){
    		var priceId= $(this).attr("href");
    		console.log(priceId);
    		var con =confirm("您确定要删除这条价格吗");
    		if(con==true){
    			$.post("${pageContext.request.contextPath}/bicyclePrice/doDeleteBicyclePrice",{priceId:priceId},function(data){
        			if(data==true||data=='true'){
        				alert("删除成功");
        				showPrice();
        			}else{
        				alert("删除失败");
        			}
        		});
    		}
    		return false;
    	});
    });
    </script>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>价格管理</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">
                            <form class="layui-form layui-col-space5">
                                <!-- <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="开始日" name="start" id="start"></div>
                                <div class="layui-input-inline layui-show-xs-block">
                                    <input class="layui-input" placeholder="截止日" name="end" id="end"></div> -->
                                <span>乘坐人数:</span>
                                <div class="layui-input-inline layui-show-xs-block" style="width: 300px;">
                                    <select id="populationSort"  xm-select="select1" xm-select-skin="normal" xm-select-height="36px">
                                    </select>
                                </div>
                                <span>使用程度:</span>
                                <div class="layui-input-inline layui-show-xs-block" style="width: 300px;">
                                    <select id ="degreeSort"  xm-select="select2" xm-select-skin="warm" xm-select-height="36px">
                                    </select>
                                </div>
                                <br/>
                                <span>租用时间:</span>
                                <div class="layui-input-inline layui-show-xs-block" style="width: 300px;">
                               		<select  xm-select="select3" id ="timeSort" xm-select-skin="danger" xm-select-height="36px">
						            </select>
                                </div>
                               <!-- <span>使用程度</span> -->
                               <!--  <div class="layui-input-inline layui-show-xs-block">
                                    <select name="contrller" id ="degreeSort">
                                        <option value="">使用程度</option>
                                        <option value="0">0年</option>
                                        <option value="1">1年</option>
                                        <option value="2">2年</option>
                                        <option value="3">3年</option>
                                        <option value="4">4年</option>
                                        </select>
                                </div> -->
                                <span>价格范围:</span>
                                <div class="layui-input-inline" style="width: 100px;">
                                  <input type="number"  placeholder="￥" autocomplete="off" class="layui-input priceMin">
                                </div>
                                <span>-</span>
                                <div class="layui-input-inline" style="width: 100px;">
                                  <input type="number"  placeholder="￥" autocomplete="off" class="layui-input priceMax">
                                </div>
                                <!-- <div class="layui-input-inline layui-show-xs-block">
                                    <input type="text" name="username" placeholder="请输入订单号" autocomplete="off" class="layui-input"></div> -->
                                <div class="layui-input-inline layui-show-xs-block">
                                    <button class="serach layui-btn"  >
                                        <i class="layui-icon">&#xe615;</i>搜索</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </form>
                        </div>
                        <div class="layui-card-header">
                        <!-- onclick="xadmin.open('添加用户','./order-add.html',800,600)" -->
                            <button class="layui-btn insertPrice" data-toggle="modal" data-target="#bicycle-price" >
                                <i class="layui-icon"></i>添加</button></div>
                        <div class="layui-card-body ">
                            <table class="layui-table layui-form">
                                <thead>
                                    <tr>
                                        <th>乘坐人数</th>
                                        <th>使用程度</th>
                                        <th>使用时间</th>
                                        <th>价格</th>
                                        <th>价格管理</th>
                                    </tr>
                                </thead>
                                <tbody class='price-body'>
                                    <!-- <tr>
                                        <td>其他方式</td>
                                        <td>申通物流</td>
                                        <td>2017-08-17 18:22</td>
                                        <td class="td-manage">
                                            <a title="查看" onclick="xadmin.open('编辑','order-view.html')" href="javascript:;">
                                                <i class="layui-icon">&#xe63c;</i></a>
                                            <a title="删除" onclick="member_del(this,'要删除的id')" href="javascript:;">
                                                <i class="layui-icon">&#xe640;</i></a>
                                        </td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="bicycle-price" aria-hidden="true" style=" opacity: 1;"
							aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
							<div class="modal-dialog modal-lg">
								<div class="modal-content"
									style="margin-top: 110px;margin-left: 170px; width: 400px; margin-top: 100px;">
									<!--<form class="avatar-form" action="upload-logo.php" enctype="multipart/form-data" method="post">-->
									<form class="avatar-form" enctype="multipart/form-data"
										method="post">
										<div class="modal-header">
											<button class="close" data-dismiss="modal" type="button">&times;</button>
											<h4 class="modal-title" id="avatar-modal-label">添加</h4>
										</div>
										<div class="modal-body">
											<br/>
											<div style='margin-left: 70px;'>
												<span>乘坐人数:</span>
				                                <div class="layui-input-inline layui-show-xs-block" >
				                                    <select id="populationSort1"  xm-select="select-popu"  xm-select-height="36px" xm-select-radio="">
				                                    </select>
				                                </div>
				                                <br/><br/>
				                                <span>使用程度:</span>
				                                <div class="layui-input-inline layui-show-xs-block" >
				                                    <select id ="degreeSort1"  xm-select="select-degr" xm-select-height="36px" xm-select-radio="">
				                                    </select>
				                                </div>
				                                <br/><br/>
				                                <span>租用时间:</span>
				                                <div class="layui-input-inline layui-show-xs-block" >
				                               		<select  xm-select="select-time" id ="timeSort1" xm-select-height="36px" xm-select-radio="">
										            </select>
				                                </div>
				                                <br/><br/>
				                                <span>价格：</span>
				                                    <div class="layui-input-inline" style="width: 100px;margin-left: 14px;">
												      <input type="text" placeholder="￥" autocomplete="off" class="layui-input price">
												      <input type="hidden" class='priceId' >
												    </div>
				                                <br/><br/>
												<div class="row">
													<div class="col-md-6">
														<button class="btn btn-danger btn-block ensure-price" type="button"
															data-dismiss="modal" style="margin-left: 36px;">确定</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
                        <!-- <div class="layui-card-body ">
                            <div class="page">
                                <div>
                                    <a class="prev" href="">&lt;&lt;</a>
                                    <a class="num" href="">1</a>
                                    <span class="current">2</span>
                                    <a class="num" href="">3</a>
                                    <a class="num" href="">489</a>
                                    <a class="next" href="">&gt;&gt;</a></div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </body>
     <script src="${pageContext.request.contextPath }/js/jquery-v3.2.1.js" type="text/javascript" charset="utf-8"></script>
     <script src="${pageContext.request.contextPath }/js/formSelects-v4.min.js" type="text/javascript"
      charset="utf-8"></script>
</html>