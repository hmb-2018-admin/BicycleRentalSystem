<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="x-admin-sm">
    
  <head>
      <meta charset="UTF-8">
      <title>自行车管理</title>
      <meta name="renderer" content="webkit">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/css/font.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath }/css/xadmin.css">
      		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/bootstrap.min.css">
       <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/formSelects-v4.css" />
      <script src="${pageContext.request.contextPath }/lib/layui/layui.js" charset="utf-8"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.3.1.min.js"></script>
      		<script src="${pageContext.request.contextPath }/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<script src="${pageContext.request.contextPath }/js/bootbox.min.js"></script>
  </head>
  <!-- 获取下拉框选项 -->
  <script type="text/javascript">
	var PIDS=new Array();//人数ids
	var DIDS=new Array();//程度ids
	var TIDS=new Array();//时间ids
  	$(function(){
  		showPopulation();
  		function showPopulation(){
  			$.post("${pageContext.request.contextPath}/populationSort/getPopulationSortAll",function(data){
  				var populationSorts =eval(data);
  				//以下代码可以在console中运行测试, 结果查看基础示例第一个
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i=0 ;i<populationSorts.length;i++){
  					PIDS[i]=populationSorts[i].populationId;
  					var population={name:populationSorts[i].sortName,value:populationSorts[i].populationId};
  					arropt[i]=population;
  				}
  				formSelects.data('select1', 'local', {arr:arropt});
  				formSelects.data('select-popu', 'local', {arr:arropt});
  			});
  		}
  		showDegreeSort();
  		function showDegreeSort(){
  			$.post("${pageContext.request.contextPath}/degreeSort/getDegreeSortAll",function(data){
  				var degreeSorts=eval(data); 
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i=0 ;i<degreeSorts.length;i++){
  					DIDS[i]=degreeSorts[i].degreeId;
  					var degreeSort={name:degreeSorts[i].degreeName,value:degreeSorts[i].degreeId};
  					arropt[i]=degreeSort;
  				}
  				formSelects.data('select2', 'local', {arr:arropt});
  				formSelects.data('select-degr', 'local', {arr:arropt});
  			});
  		}
  		showTimeSort();
  		function showTimeSort(){
  			$.post("${pageContext.request.contextPath}/timeSort/getTimeSortAll",function(data){
  				var timeSorts=eval(data);
  				var formSelects = layui.formSelects;
  				var arropt=new Array();
  				for(var i= 0; i < timeSorts.length ; i++){
  					TIDS[i]=timeSorts[i].timeId;
  					var timeSort ={name:timeSorts[i].timeName,value:timeSorts[i].timeId};
  					arropt[i]=timeSort;
  				}
  				formSelects.data('select3', 'local', {arr:arropt});
  				formSelects.data('select-time', 'local', {arr:arropt});
  			});
  		}
  	});
  </script>
  <script type="text/javascript">
  	var states='';
	function showBicycle(){
  		//获取使用人数的值
  		var formSelects = layui.formSelects;
  		var populationIds=formSelects.value('select1','valStr'); 
  		var degreeIds=formSelects.value('select2','valStr'); 
  		var timeIds=formSelects.value('select3','valStr'); 
  		var priceMax=$(".priceMax").val();
  		var priceMin=$(".priceMin").val();
  		var brand =$(".brand").val();
  		var param={
  				degreeIds:degreeIds,
  				timeIds:timeIds,
  				populationIds:populationIds,
  				priceMax:priceMax,
  				priceMin:priceMin,
  				brand:brand,
  				states:states
  				
  		}
  		$.post("${pageContext.request.contextPath}/bicycle/getBicycleByQueryCondition",param,function(data){
		var bicycle=eval(data); 
		var tr="";
		for(var i = 0 ; i < bicycle.length ; i++){
			var state="";
			if(bicycle[i].state==1){
				state="等待租赁";
			}else if(bicycle[i].state==2){
				state="正在租赁";
			}else if(bicycle[i].state==3){
				state="报修";
			}else{
				state="正在维修";
			}
			tr+="<tr><td>"+bicycle[i].bicycleNumber+
			"</td><td>"+bicycle[i].brand+
			"</td><td>"+bicycle[i].bicyclePrice.populationSort.sortName+
			"</td><td>"+bicycle[i].bicyclePrice.degreeSort.degreeName+
			"</td><td>"+bicycle[i].bicyclePrice.timeSort.timeName+
			"</td><td> ￥"+bicycle[i].bicyclePrice.price+
			"</td><td> "+state+
			"</td><td><a title='修改'  href ='"+bicycle[i].bicycleId+"'  data-toggle='modal' data-target='#bicycle' class='updatePrice'>"+
                  "<i class='iconfont' style='font-size: 22px;'>&#xe69e;</i></a>"+
                  "<a title='删除'   href ='"+bicycle[i].bicycleId+"' class='deleteBicycle'>"+
                  "<i class='layui-icon' style='font-size: 22px;'>&#xe640;</i></a></td></tr>";
		}
		$(".bicycle-body").html(tr);
  		});
  	}
	function insertBicycle(state){
		var formSelects = layui.formSelects;
  		var populationId=formSelects.value('select-popu','valStr'); 
  		var degreeId=formSelects.value('select-degr','valStr'); 
  		var timeId=formSelects.value('select-time','valStr');
  		var brand = $(".modal-brand").val();
  		if(populationId==null||populationId==''){
  			alert("乘坐人数不能为空");
  			return false;
  		}
  		if(degreeId==null||degreeId==''){
  			alert("使用程度不能为空");
  			return false;
  		}
  		if(timeId==null||timeId==''){
  			alert("使用时间不能为空");
  			return false;
  		}
  		if(brand==null||brand==''){
  			alert("品牌不能为空");
  			return false;
  		}
  		var param={
  				populationId:populationId,
  				degreeId:degreeId,
  				timeId:timeId
  		}
  		$.post("${pageContext.request.contextPath}/bicyclePrice/verifyBicyclePrice",param,function(data){
  			if(data== null ||data==''){
  				alert("该组合的价格不存在，请前往设置价格");
  				return false;
  			}else{
  				var param2={
  						priceId:data,
  						brand:brand,
  						state:state
  				}
  				$.post("${pageContext.request.contextPath}/bicycle/doInsertBicycle",param2,function(date){
  					if(date==true||date=='true'){
  						alert("添加成功");
  						formSelects.value('select1', PIDS, false);
  	    	    		formSelects.value('select2', DIDS, false);
  	    	    		formSelects.value('select3', TIDS, false);
  	    	    		$(".priceMax").val('');
  	    	    		$(".priceMin").val('');
  	    	    		$(".brand").val('');
  						showBicycle();
  					}else{
  						alert("添加失败");
  					}
  				})
  			}
  		});
	}
	function updateBicycle(state){
		/* if(state==2){
			alert("正在租赁的车无法修改");
			return false;
		} */
		var formSelects = layui.formSelects;
  		var populationId=formSelects.value('select-popu','valStr'); 
  		var degreeId=formSelects.value('select-degr','valStr'); 
  		var timeId=formSelects.value('select-time','valStr');
  		var brand = $(".modal-brand").val();
  		var bicycleId=$(".bicycleId").val();
  		var bicycleNumber=$(".bicycleNumber").val();
  		if(populationId==null||populationId==''){
  			alert("乘坐人数不能为空");
  			return false;
  		}
  		if(degreeId==null||degreeId==''){
  			alert("使用程度不能为空");
  			return false;
  		}
  		if(timeId==null||timeId==''){
  			alert("使用时间不能为空");
  			return false;
  		}
  		if(brand==null||brand==''){
  			alert("品牌不能为空");
  			return false;
  		}
  		var param={
  				populationId:populationId,
  				degreeId:degreeId,
  				timeId:timeId
  		}
  		$.post("${pageContext.request.contextPath}/bicyclePrice/verifyBicyclePrice",param,function(data){
  			if(data== null ||data==''){
  				alert("该组合的价格不存在，请前往设置价格");
  				return false;
  			}else{
  				var param2={
  						priceId:data,
  						brand:brand,
  						state:state,
  						bicycleId:bicycleId,
  						bicycleNumber:bicycleNumber
  				}
  				$.post("${pageContext.request.contextPath}/bicycle/doUpdateBicycle",param2,function(date){
  					if(date==true||date=='true'){
  						alert("修改成功");
  						formSelects.value('select1', PIDS, false);
  	    	    		formSelects.value('select2', DIDS, false);
  	    	    		formSelects.value('select3', TIDS, false);
  	    	    		$(".priceMax").val('');
  	    	    		$(".priceMin").val('');
  	    	    		$(".brand").val('');
  						showBicycle();
  					}else{
  						alert("修改失败");
  					}
  				})
  			}
  		});
	}
  $(function(){
	showBicycle();
  	//弹出添加框
  	$(document).on("click",".insertPrice",function(){
  		$(".ensure-price").attr("lay-filter","insertForm");
  		$("#avatar-modal-label").html("添加自行车信息");
  		$(".number").css({"display":"none"});
  		var formSelects = layui.formSelects;
  		formSelects.value('select-popu', PIDS, false);
  		formSelects.value('select-degr', DIDS, false);
  		formSelects.value('select-time', TIDS, false);
  		$(".modal-brand").val('');
  	});
  	//弹出修改框
  	$(document).on("click",".updatePrice",function(){
  		$("#avatar-modal-label").html("修改自行车信息");
  		$(".ensure-price").attr("lay-filter","updateForm");
  		var bicycleId=$(this).attr("href");
  		var formSelects = layui.formSelects;
  		$(".number").css({"display":"block"});
  		$.post("${pageContext.request.contextPath}/bicycle/getBicycleByBicycleId",{bicycleId:bicycleId},function(data){
  			var bicycle=eval(data);
  			console.log(bicycle);
  			$(".bicycleId").val(bicycle.bicycleId);
  			formSelects.value('select-popu', [bicycle.bicyclePrice.populationId], true);
	   		formSelects.value('select-degr', [bicycle.bicyclePrice.degreeId], true);
	   		formSelects.value('select-time', [bicycle.bicyclePrice.timeId], true);
	   		$(".modal-brand").val(bicycle.brand); 
	   		$(".bicycleNumber").val(bicycle.bicycleNumber);
	   		$("input[name='state'][value="+bicycle.state+"]").prop("checked",true);
	   		layui.use('form', function(){
	   		  var form = layui.form;
	   		  form.render('radio');
	   		});
	  		});
	  		return false;
  	});
  	
  	//删除价格
  	$(document).on("click",".deleteBicycle",function(){
  		var bicycleId= $(this).attr("href");
  		var con =confirm("您确定要删除这辆自行车吗");
  		if(con==true){
  			$.post("${pageContext.request.contextPath}/bicycle/doDeleteBicycle",{bicycleId:bicycleId},function(data){
      			if(data==true||data=='true'){
      				alert("删除成功");
      				showBicycle();
      			}else{
      				alert("删除失败");
      			}
      		});
  		}
  		return false;
  	});
  });
  </script>
  <style>
  .content span{
  	font-size: 14px;
    margin-left: 20px;
  }
  </style>
<body>
	<div class="x-nav">
		<span class="layui-breadcrumb"> <a href="">首页</a> <a> <cite>自行车管理</cite></a>
		</span> <a class="layui-btn layui-btn-small"
			style="line-height: 1.6em; margin-top: 3px; float: right"
			onclick="location.reload()" title="刷新"> <i
			class="layui-icon layui-icon-refresh" style="line-height: 30px"></i>
		</a>
	</div>
	<div class="layui-fluid">
		<div class="layui-row layui-col-space15">
			<div class="layui-col-md12">
				<div class="layui-card">
					<div class="layui-card-body ">
						<div class="layui-collapse" lay-filter="test">
							<div class="layui-colla-item">
								<h2 class="layui-colla-title">
									条件筛选<i class="layui-icon layui-colla-icon"></i>
								</h2>
								<div class="layui-colla-content">
									<form class="layui-form" action="">
										<div class="layui-form-item">
											<div class="layui-inline">
												<label class="layui-form-label">价格范围</label>
												<div class="layui-input-inline" style="width: 100px;">
													<input type="text" name="priceMin" placeholder="￥"
														autocomplete="off" class="layui-input priceMin">
												</div>
												<div class="layui-form-mid">-</div>
												<div class="layui-input-inline" style="width: 100px;">
													<input type="text" name="priceMax" placeholder="￥"
														autocomplete="off" class="layui-input priceMax">
												</div>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">乘车人数</label>
											<div class="layui-input-inline">
												<select id="populationSort" xm-select="select1"
													xm-select-skin="normal" xm-select-height="36px" name='populationIds'>
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">使用程度</label>
											<div class="layui-input-inline">
												<select id="degreeSort"  xm-select="select2"
													xm-select-skin="warm" xm-select-height="36px" name='degreeIds'>
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">使用时间</label>
											<div class="layui-input-inline">
												<select xm-select="select3" id="timeSort"
													xm-select-skin="danger" xm-select-height="36px" name='timeIds' >
												</select>
											</div>
										</div>
										<div class="layui-form-item">
											<div class="layui-inline">
												<label class="layui-form-label">品牌:</label>
												<div class="layui-input-inline">
													<input type="text" class='brand layui-input' name='brand'>
												</div>
											</div>
										</div>
										<div class="layui-form-item">
											<label class="layui-form-label">车辆状态</label>
											<div class="layui-input-block" style="margin-left: -1px;">
												<input type="checkbox" name='state1' value="1" title="等待租赁">
												<input type="checkbox" name='state2' value="2" title="正在租赁">
												<input type="checkbox" name='state3' value="3" title="报修">
												<input type="checkbox" name='state4' value="4" title="正在维修">
											</div>
										</div>
										<div class="layui-form-item">
											<div class="layui-input-block">
												<button class="serach layui-btn" lay-submit
													lay-filter="serachForm">
													<i class="layui-icon">&#xe615;</i>搜索
												</button>
												<button type="reset" class="layui-btn layui-btn-primary">重置</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="layui-card-header">
						<!-- onclick="xadmin.open('添加用户','./order-add.html',800,600)" -->
						<button class="layui-btn insertPrice" data-toggle="modal"
							data-target="#bicycle">
							<i class="layui-icon"></i>添加
						</button>
					</div>
					<div class="layui-card-body ">
						<table class="layui-table layui-form">
							<thead>
								<tr>
									<th>自行车编号</th>
									<th>品牌</th>
									<th>乘坐人数</th>
									<th>使用程度</th>
									<th>使用时间</th>
									<th>价格</th>
									<th>状态</th>
									<th>自行车管理</th>
								</tr>
							</thead>
							<tbody class='bicycle-body'>
							</tbody>
						</table>
					</div>
					<div class="modal fade" id="bicycle" aria-hidden="true"
						style="opacity: 1;" aria-labelledby="avatar-modal-label"
						role="dialog" tabindex="-1">
						<div class="modal-dialog modal-lg">
							<div class="modal-content"
								style="width: 700px; margin-top: 100px;">
								<div class="modal-header">
									<button class="close" data-dismiss="modal" type="button">&times;</button>
									<h4 class="modal-title" id="avatar-modal-label">添加</h4>
								</div>
								<form class="layui-form" action="">
								<div class="modal-body">
									
									<div style='margin-left: 70px;' class='content'>
										<div class='number' style='display:none'>
										<span>自行车编号:</span>
										<div class="layui-input-inline">
											<input type="text" class='bicycleNumber layui-input' disabled >
										</div>
										<br/><br />
										</div>
										<span>乘坐人数:</span>
										<div class="layui-input-inline layui-show-xs-block">
											<select id="populationSort1" xm-select="select-popu"
												xm-select-height="36px" xm-select-radio="">
											</select>
										</div>
										<span>使用程度:</span>
										<div class="layui-input-inline layui-show-xs-block">
											<select id="degreeSort1" xm-select="select-degr"
												xm-select-height="36px" xm-select-radio="">
											</select>
										</div>
										<br /> <br /> <span>租用时间:</span>
										<div class="layui-input-inline layui-show-xs-block">
											<select xm-select="select-time" id="timeSort1"
												xm-select-height="36px" xm-select-radio="">
											</select>
										</div>
										<span>品牌：</span>
										<div class="layui-input-inline"
											style=" margin-left: 14px;">
											<input type="text"  autocomplete="off"
												class="layui-input modal-brand"> 
										</div>
										<br /> <br /> <span>车辆状态：</span>
										<div class="layui-input-inline"
											>
											<input type="radio" name="state" value="1" title="等待租赁" checked>
											<input type="radio" name="state" value="2" title="正在租赁" >
											<input type="radio" name="state" value="3" title="报修" >
											<input type="radio" name="state" value="4" title="正在维修" >
											<input type="hidden" class='bicycleId'>
										</div>
										<br /> <br />
										<div class="row">
											<div class="col-md-2">
												<button class="btn btn-danger btn-block ensure-price"
													type="button" data-dismiss="modal"  lay-submit
													lay-filter
													style="margin-left: 200px;">确定</button>
											</div>
										</div>
									</div>
								</div>
							</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="${pageContext.request.contextPath }/js/jquery-v3.2.1.js"
	type="text/javascript" charset="utf-8"></script>
<script
	src="${pageContext.request.contextPath }/js/formSelects-v4.min.js"
	type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
layui.use(['laydate', 'form'],
	        function() {
	            var laydate = layui.laydate;

	            //执行一个laydate实例
	            laydate.render({
	                elem: '#start' //指定元素
	            });
	            
	            //执行一个laydate实例
	            laydate.render({
	                elem: '#end' //指定元素
	            });
	            
	            var form = layui.form;
	            
	            //form.render(); 
	            
	            //监听提交
	            form.on('submit(serachForm)', function(data){
	              states='';
	              if(data.field.state1 != undefined){
	            	  states+=data.field.state1+","
	              }
	              if(data.field.state2 != undefined){
	            	  states+=data.field.state2+","
	              }
	              if(data.field.state3 != undefined){
	            	  states+=data.field.state3+","
	              }
	              if(data.field.state4 != undefined){
	            	  states+=data.field.state4+","
	              }
	              showBicycle();
	              $(".layui-colla-title").click();
	              return false;
	            });
	            //添加表单的提交
	            form.on("submit(insertForm)",function (data){
	            	var state=data.field.state
	            	insertBicycle(state);
	            });
	            //添加表单的提交
	            form.on("submit(updateForm)",function (data){
	            	var state=data.field.state
	            	updateBicycle(state);
	            });
	        });
</script>
</html>