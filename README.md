# 自行车租赁系统

#### 介绍
本项目主要由前台模块和后台模块组成，前台主要有用户的登陆，注册，租赁自行车，还车，支付，后台模块主要有管理员管理，自行车管理，价格管理，优惠券管理等功能。

#### 软件架构
主要技术：Nginx，Keepalived，Dubbo，ZooKeeper，SpringBoot ，MySQL，SSM


#### 实现效果
### 后台模块效果
 **自行车分类管理页面** 
![自行车分类管理页面](https://images.gitee.com/uploads/images/2020/0202/160306_bf10ebf5_5486861.png "屏幕截图.png")
修改自行车图片类别
![修改自行车图片](https://images.gitee.com/uploads/images/2020/0202/161340_618b9937_5486861.png "屏幕截图.png")
修改使用程度类别
![修改使用程度类别](https://images.gitee.com/uploads/images/2020/0202/161418_71ecbefc_5486861.png "屏幕截图.png")
修改使用时间类别
![修改使用时间类别](https://images.gitee.com/uploads/images/2020/0202/161535_c91fe014_5486861.png "屏幕截图.png")

**自行车价格管理页面** 
![自行车价格管理页面](https://images.gitee.com/uploads/images/2020/0202/160433_444f0fef_5486861.png "屏幕截图.png")
修改价格
![修改价格](https://images.gitee.com/uploads/images/2020/0202/161633_213f3bc5_5486861.png "屏幕截图.png")

 **自行车管理页面** 
![自行车管理页面](https://images.gitee.com/uploads/images/2020/0202/160608_5684c875_5486861.png "屏幕截图.png")
修改自行车信息
![修改自行车信息](https://images.gitee.com/uploads/images/2020/0202/161721_427ba34b_5486861.png "屏幕截图.png")
 **自行车报修管理页面** 
![自行车报修管理页面](https://images.gitee.com/uploads/images/2020/0202/160635_eb021361_5486861.png "屏幕截图.png")
故障自行车进行报修
![报修自行车](https://images.gitee.com/uploads/images/2020/0202/161829_8f3b6aa5_5486861.png "屏幕截图.png")
维修好的自行车进行使用
![使用自行车](https://images.gitee.com/uploads/images/2020/0202/161902_32718a82_5486861.png "屏幕截图.png")
 **自行车信息追踪页面**
![自行车信息追踪](https://images.gitee.com/uploads/images/2020/0202/162055_0b3d8693_5486861.png "屏幕截图.png") 
自行车信息详情
![自行车信息详情](https://images.gitee.com/uploads/images/2020/0202/162131_f7e67480_5486861.png "屏幕截图.png")
![对自行车的使用进行管理](https://images.gitee.com/uploads/images/2020/0202/162208_1fc6715f_5486861.png "屏幕截图.png")
 **自行车卡券管理页面** 
卡券管理
![卡券管理](https://images.gitee.com/uploads/images/2020/0202/162330_0b39defa_5486861.png "屏幕截图.png")
添加卡券
![添加卡券](https://images.gitee.com/uploads/images/2020/0202/162416_c82a585c_5486861.png "屏幕截图.png")
修改卡券信息
![修改卡券信息](https://images.gitee.com/uploads/images/2020/0202/162449_f81c01dd_5486861.png "屏幕截图.png")
 **管理员管理页面** 
管理员管理页面
![管理员管理](https://images.gitee.com/uploads/images/2020/0202/162551_3b882e4d_5486861.png "屏幕截图.png")
修改管理信息
![修改管理员信息](https://images.gitee.com/uploads/images/2020/0202/162633_6bce076b_5486861.png "屏幕截图.png")
重置管理员密码

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
