package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.DegreeSortDao;
import com.entities.DegreeSort;
import com.service.DegreeSortService;

@Service
@Component
public class DegreeSortServiceImp implements DegreeSortService {
	
	@Autowired
	private DegreeSortDao degreeSortDao;
	
	@Transactional(readOnly =  true)
	@Override
	public List<DegreeSort> getDegreeSortAll() {
		return this.degreeSortDao.getDegreeSortAll();
	}
	@Transactional()
	@Override
	public boolean doInsertDegreeSort(DegreeSort DegreeSort) {
		boolean flag=false;
		try {
			this.degreeSortDao.doInsertDegreeSort(DegreeSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean doUpdateDegreeSort(DegreeSort DegreeSort) {
		boolean flag=false;
		try {
			this.degreeSortDao.doUpdateDegreeSort(DegreeSort);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean doDeleteDegreeSort(Integer degreeId) {
		boolean flag=false;
		try {
			this.degreeSortDao.doDeleteDegreeSort(degreeId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	@Transactional(readOnly =  true)
	@Override
	public DegreeSort getDegreeSortByDegreeId(Integer degreeId) {
		return this.degreeSortDao.getDegreeSortByDegreeId(degreeId);
	}

}
