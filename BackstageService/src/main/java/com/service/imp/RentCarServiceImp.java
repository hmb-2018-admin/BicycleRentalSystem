package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.BicycleDao;
import com.dao.RentCarDao;
import com.entities.RentCar;
import com.service.RentCarService;

@Service
@Component
public class RentCarServiceImp implements RentCarService {
	@Autowired
	private RentCarDao rentCarDao;
	@Autowired
	private BicycleDao bicycleDao;
	
	@Transactional(readOnly = true)
	@Override
	public List<RentCar> getRentCarByBicycleIdAndStartDateAndExpiryDate(Integer bicycleId, String startDate,
			String expiryDate) {
		return this.rentCarDao.getRentCarByBicycleIdAndStartDateAndExpiryDate(bicycleId, startDate, expiryDate);
	}
	@Transactional()
	@Override
	public boolean doReturnCar(Integer rentId) {
		boolean flag=false;
		try {
		RentCar rentCar=this.rentCarDao.getRentCarByRentId(rentId);
		this.bicycleDao.doUpdateStateByBicycleIdAndState(rentCar.getBicycleId(), 1);
		this.rentCarDao.doReturnCar(rentId);
		flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

}
