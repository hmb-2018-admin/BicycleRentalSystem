package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.AdminDao;
import com.entities.Admin;
import com.service.AdminService;
@Component
@Service
public class AdminServiceImp implements AdminService{
	@Autowired
	private AdminDao adminDao;
	@Transactional(readOnly = true)
	@Override
	public Admin loginAdmin(String adminName, String adminPassword) {
		return this.adminDao.loginAdmin(adminName, adminPassword);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Admin getAdminByAdminId(Integer adminId) {
		return this.adminDao.getAdminByAdminId(adminId);
	}
	@Transactional()
	@Override
	public boolean doInsertAdmin(Admin admin) {
		boolean flag=false;
		try {
			this.adminDao.doInsertAdmin(admin);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional()
	@Override
	public boolean doUpdateAdmin(Admin admin) {
		boolean flag=false;
		try {
			this.adminDao.doUpdateAdmin(admin);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional()
	@Override
	public boolean doDeleteAdmin(Integer adminId) {
		boolean flag=false;
		try {
			this.adminDao.doDeleteAdmin(adminId);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	@Transactional(readOnly = true)
	@Override
	public List<Admin> getAdminByAdminNameLike(String adminName) {
		return this.adminDao.getAdminByAdminNameLike("%"+adminName+"%");
	}

}
