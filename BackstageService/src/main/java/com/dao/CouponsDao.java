package com.dao;

import java.util.List;

import com.entities.Coupons;

public interface CouponsDao {
	//根据查询条件来查询优惠券
	public List<Coupons> getCouponsByQuery(String startDate,String expiryDate,Double priceMin,Double priceMax );
	
	//添加优惠券
	public void doInsertCoupons(Coupons coupons);
	
	//修改优惠券
	public void doUpdateCoupons(Coupons coupons);
	
	//通过id获取优惠券的信息
	public Coupons getCouponsByCouponsId(Integer couponsId);
	
	//删除优惠券
	public void doDeleteCoupons(Integer couponsId);
}
