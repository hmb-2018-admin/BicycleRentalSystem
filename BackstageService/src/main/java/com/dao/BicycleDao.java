package com.dao;

import java.util.List;

import com.entities.Bicycle;

public interface BicycleDao {
	
	//通过priceId 和品牌和状态来查询自行车
	public List<Bicycle> getBicycleByPriceIdsAndLikeBand(List<Integer> priceIds,String brand,String [] state);
	
	//添加新的自行车
	public void doInsertBicycle(Bicycle bicycle);
	
	//修改自行车信息
	public void doUpdateBicycle(Bicycle bicycle);
	
	//删除自行车信息
	public void doDeleteBicycle(Integer bicycleId);
	
	//通过品牌获取该品牌下的最大自行车编号
	public String getBicycleNumberByBrand(String brand);
	
	//根据自行车id获取自行车信息
	public Bicycle getBicycelByBicycleId(Integer bicycleId);
	
	//更改自行车状态
	public void doUpdateStateByBicycleIdAndState(Integer bicycleId, Integer state);
}
