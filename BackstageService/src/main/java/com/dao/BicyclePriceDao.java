package com.dao;

import java.util.List;

import com.entities.BicyclePrice;

public interface BicyclePriceDao {
	//查询价格表信息
	public List<BicyclePrice> getBicyclePriceByPopulationIdsAndDegreeIdsAndTimeIdsAndPriceMaxAndPriceMin(String [] populationIds,String [] degreeIds,String [] timeIds ,Double priceMin,Double priceMax);
	
	//添加新的价格
	public void doInsertBicyclePrice(BicyclePrice bicyclePrice);
	
	//修改价格
	public void doUpdateBicyclePrice(BicyclePrice bicyclePrice);
	
	//通过id获取价格信息
	public BicyclePrice getBicyclePriceByPriceId(Integer priceId);
	
	//删除价格
	public void doDeleteBicyclePrice(Integer priceId);
	
	//通过bicyclePrice获取价格信息
	public BicyclePrice getBicyclePriceByBicyclePrice(BicyclePrice bicyclePrice);
		
}
