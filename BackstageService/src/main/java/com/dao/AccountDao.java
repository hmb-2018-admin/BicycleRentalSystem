package com.dao;

import com.entities.Account;

public interface AccountDao {
	//通过用户id获取用户信息
	public Account getAccountByAccountId(Integer accountId);
}
