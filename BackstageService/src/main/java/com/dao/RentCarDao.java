package com.dao;

import java.util.List;

import com.entities.RentCar;

public interface RentCarDao {
	//获取自行车的租车信息
	public List<RentCar> getRentCarByBicycleIdAndStartDateAndExpiryDate(Integer bicycleId, String startDate,String expiryDate);
	
	//立即还车
	public void doReturnCar(Integer rentId);
	
	//根据id获取租车信息
	public RentCar getRentCarByRentId(Integer rentId);
}
