package com.dao;

import java.util.List;

import com.entities.TimeSort;

public interface TimeSortDao {
	//显示所有的程度分类
	public List<TimeSort> getTimeSortAll();
	
	//添加新的程度分类
	public void doInsertTimeSort(TimeSort timeSort);
	
	//修改分类信息
	public void doUpdateTimeSort(TimeSort timeSort);
	
	//通过id获取分类信息
	public TimeSort getTimeSortByTimeId(Integer timeId);
	
	//删除分类
	public void doDeleteTimeSort(Integer timeId);
}
