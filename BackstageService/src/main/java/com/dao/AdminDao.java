package com.dao;

import java.util.List;

import com.entities.Admin;

public interface AdminDao {
	//管理员登陆
	public Admin loginAdmin(String adminName,String adminPassword);
	
	//获取所有的管理员信息
	public List<Admin> getAdminByAdminNameLike(String adminName);
	
	//通过账号id获取用户信息
	public Admin getAdminByAdminId(Integer adminId);
	
	//添加管理员
	public void doInsertAdmin(Admin admin);
	
	//修改管理员
	public void doUpdateAdmin(Admin admin);
	
	//删除管理员
	public void doDeleteAdmin(Integer adminId);
}
