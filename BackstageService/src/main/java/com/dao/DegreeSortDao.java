package com.dao;

import java.util.List;

import com.entities.DegreeSort;

public interface DegreeSortDao {
	//显示所有的程度分类
	public List<DegreeSort> getDegreeSortAll();
	
	//添加新的程度分类
	public void doInsertDegreeSort(DegreeSort degreeSort);
	
	//修改分类信息
	public void doUpdateDegreeSort(DegreeSort degreeSort);
	
	//通过id获取分类信息
	public DegreeSort getDegreeSortByDegreeId(Integer degreeId);
	
	//删除分类
	public void doDeleteDegreeSort(Integer degreeId);
}
