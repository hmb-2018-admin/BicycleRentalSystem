package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;

@SpringBootApplication
@EnableDubbo
@MapperScan("com.dao")
public class BackstageServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackstageServiceApplication.class, args);
	}

}
