package com.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.dao.AccountDao;
import com.entities.Account;
import com.service.AccountService;

@Service
@Component
public class AccountServiceImp implements AccountService {
	@Autowired
	private AccountDao accountDao;
	
	@Transactional(readOnly = true)
	@Override
	public Account loginAccount(String accountName, String accountPassword) {
		return this.accountDao.loginByAccountNameAndAccountPassword(accountName, accountPassword);
	}

	@Transactional(readOnly = true)
	@Override
	public boolean checkAccountName(String accountName) {
		boolean flag=true;
		if(this.accountDao.checkAccountName(accountName)>=1) {
			flag=false;
		}
		return flag;
	}

	@Transactional(readOnly = true)
	@Override
	public boolean checkPhone(String phone) {
		boolean flag=true;
		if(this.accountDao.checkPhone(phone)>=1) {
			flag=false;
		}
		return flag;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean checkIdCard(String idCard) {
		boolean flag=true;
		if(this.accountDao.checkIdCard(idCard)>=1) {
			flag=false;
		}
		return flag;
	}

	@Transactional()
	@Override
	public boolean registerAccount(Account account) {
		boolean flag=false;
		try {
			this.accountDao.registerAccount(account);
			flag=true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
}
